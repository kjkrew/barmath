--[[
	BarMath
	Main Functions
	Version <%version%>
	
	Revision: $Id: Main.lua 12 2012-12-23 11:47:41 PST Kjasi $
]]

local bm = _G.BarMath
if (not bm) then
	print(RED_FONT_COLOR_CODE.."Main is unable to find BarMath's Global.|r")
	return
end

local L = bm.Localize
if (not L) then
	print(RED_FONT_COLOR_CODE.."Main is unable to find BarMath's Localization.|r")
	return
end

--== Variables ==--
bm.Version = "<%version%>"
bm.InCombat = false

--Bar List
BarMath_Barcount = 0
BarMath_BarList = {}
BarMath_EventList = {}
BarMath_BarInfo = {}
BarMath_BarInfoDelete = {}
BarMath_HaveLoaded = {
	["Addons"] = {},
	["Bars"] = {},
}

local BarMath_Color_01 = "|CFF65DF65" -- Standard BarMath Color
local BarMath_Color_02 = "|CFF5555FF" -- Debug Color
local BarMath_NewPlayer = false

BarMath_Realm = GetRealmName()
BarMath_Faction = UnitFactionGroup("player")
BarMath_PlayerName = UnitName("player")
BarMath_Class, BarMath_EngClass = UnitClass("player")

local BarMath_UI_Bkup = UIPARENT_MANAGED_FRAME_POSITIONS

-- Defaults
local BarMath_Defaults = {
	["Options"] = {
		["Version"] = bm.Version,
		["Rounding"] = BARMATH_ROUNDING_DOWN, -- Other Values: BARMATH_ROUNDING_UP, BARMATH_ROUNDING_STANDARD
		["Decimals"] = 2,
		["ShowTooltips"] = 1, -- 1 = On, 0 = Off
		["ShowParentBar"] = 0,
		["ShowMinimapButton"] = 1,
		["ShowOverBar"] = 1,
		["TrimNumbers"] = 0,
		["CharListShowOtherFactions"] = 1,
		["CharListShowOtherServers"] = 1,
		["AllowSnapToZones"] = 1,
		["UseCommas"] = 1,
		["ToonData"] = {
			["Race"] = UnitRace("player"),
			["Class"] = UnitClass("player"),
			["Level"] = UnitLevel("player"),
			["Sex"] = UnitSex("player"),
		},
		["Bars"] = {},
	},
	["Bars"] = {					-- Default settings for Bars. Used if custom options not specified.
		["Display"] = 1,			-- 1 = Show Bar, 0 = Hide Bar
		["DisplayMethod"] = 1,			-- 1 = Text is Always on, 0 = Text only when mouseover
		["TextLength"] = BARMATH_BARTEXT_SHORT,	-- Length of text on the Bars. Other Value: BARMATH_BARTEXT_LONG
		["Displays"] = {
			["Display1"] = 1,		-- Determines the default option for the Displays.
			["Display2"] = 2,
			["Display3"] = 3,
			["Display4"] = 4,
			["Display5"] = 5,
			["Display1OnOff"] = 1,		-- Default setting for if this Display is visible. 1 = Visible, 0 = Hidden.
			["Display2OnOff"] = 1,
			["Display3OnOff"] = 1,
			["Display4OnOff"] = 1,
			["Display5OnOff"] = 1,
		},
	},
}

--== Basic Functions ==--
function BarMath_Load(self)
	self:RegisterEvent("ADDON_LOADED")
	self:RegisterEvent("PLAYER_LEVEL_UP")
	self:RegisterEvent("PLAYER_REGEN_DISABLED")
	self:RegisterEvent("PLAYER_REGEN_ENABLED")

	-- Slash Commands
	SLASH_BarMath1 = "/barmath"
	SLASH_BarMath2 = "/bm"
	SlashCmdList["BarMath"] = BarMath_commandline
end

function BarMath_OnEvent(self, event, ...)
	local arg1, arg2, arg3 = ...

	if (event == "ADDON_LOADED") then
		BarMath_Loaded(self)
		self:UnregisterEvent("ADDON_LOADED")
	elseif event == "PLAYER_LEVEL_UP" then
		BarMath_SetCharVar(arg1, "ToonData", "Level")
	elseif (event == "PLAYER_REGEN_DISABLED") then
		bm.InCombat = true
	elseif (event == "PLAYER_REGEN_ENABLED") then
		bm.InCombat = false
	else
		for k,v in pairs(BarMath_EventList) do
			if (event == k) then
				for y=1,#v do
					v[y](...)
				end
			end
		end
	end
end

function BarMath_Loaded(self)
	BarMath_BuildDB()

	for k,v in pairs(BarMath_EventList) do
		self:RegisterEvent(k)
		--BarMath_Msg("Event Registered: \""..k.."\"")
	end

	BarMath_ParentBar_SetData()
	BarMath_ParentBar_SetPos()

	BarMath_Msg(format(BARMATH_MSG_LOADED,bm.Version))
end


--== Bar Management Functions ==--

function BarMath_ParentBar_SetHeight()
	if (bm.InCombat==true) then return end
	local bars = 0
	if (BarMath_GetCharVar("ShowParentBar") == 1) then
		bars = bars + 1
	end

	for k,v in pairs(BarMath_BarList) do
		if _G["BarMath"..v["Name"].."Bar"]:IsVisible() then
			bars = bars + 1
		end
	end

	BarMath_ParentFrame:SetHeight(bm.BarHeight*bars)
end

function BarMath_ParentBar_SetData()
	BarMathTitleText:SetText(format(BARMATH_BAR_TITLE,bm.Version))

	-- Manage Blizzard Frames
	-- Action Bars
	UIPARENT_MANAGED_FRAME_POSITIONS["MultiBarBottomLeft"]["anchorTo"] = "BarMath_ParentFrame"
	UIPARENT_MANAGED_FRAME_POSITIONS["MultiBarBottomLeft"]["baseY"] = 4
	UIPARENT_MANAGED_FRAME_POSITIONS["MultiBarBottomLeft"]["reputation"] = 0
	UIPARENT_MANAGED_FRAME_POSITIONS["MultiBarBottomLeft"]["maxLevel"] = 0
	-- Shapeshift, Possess, Pet & Cast Bars
	UIPARENT_MANAGED_FRAME_POSITIONS["StanceBarFrame"]["reputation"] = 0
	UIPARENT_MANAGED_FRAME_POSITIONS["StanceBarFrame"]["maxLevel"] = 0
	UIPARENT_MANAGED_FRAME_POSITIONS["PossessBarFrame"]["reputation"] = 0
	UIPARENT_MANAGED_FRAME_POSITIONS["PossessBarFrame"]["maxLevel"] = 0
	UIPARENT_MANAGED_FRAME_POSITIONS["MultiCastActionBarFrame"]["reputation"] = 0
	UIPARENT_MANAGED_FRAME_POSITIONS["MultiCastActionBarFrame"]["maxLevel"] = 0
end

function BarMath_ParentBar_SetPos()
	if (bm.InCombat==true) then return end
	BarMath_ParentBar_SetHeight()
	BarMath_ParentFrame:ClearAllPoints()

	local BarMathYOffset = BarMath_ParentFrame:GetHeight()-bm.BarHeight

	if (not UIPARENT_MANAGED_FRAME_POSITIONS["BarMath_ParentFrame"]) then
		UIPARENT_MANAGED_FRAME_POSITIONS["BarMath_ParentFrame"] = {
			["baseY"] = 0,
			["bottomLeft"] = 0, 
			["anchorTo"] = "ActionButton1",
			["point"] = "BOTTOMLEFT", 
			["rpoint"] = "TOPLEFT",
			["xOffset"] = -8,
			["yOffset"] = 2,
		}
	end
	-- Action Bars
	UIPARENT_MANAGED_FRAME_POSITIONS["MultiBarBottomLeft"]["anchorTo"] = "BarMath_ParentFrame"
	-- Shapeshift/Possess Bars
	UIPARENT_MANAGED_FRAME_POSITIONS["StanceBarFrame"]["yOffset"] = BarMathYOffset
	UIPARENT_MANAGED_FRAME_POSITIONS["PossessBarFrame"]["yOffset"] = BarMathYOffset
	UIPARENT_MANAGED_FRAME_POSITIONS["MultiCastActionBarFrame"]["yOffset"] = BarMathYOffset
	UIPARENT_MANAGED_FRAME_POSITIONS["PETACTIONBAR_YPOS"]["baseY"] = 97+BarMathYOffset

	UIParent_ManageFramePositions()
	--updateContainerFrameAnchors()
end

-- Re-Orders the bars.
-- Direction is used for reversing the Bar Order. Values = 1 or -1
function BarMath_ReorderBars(direction)
	if (bm.InCombat==true) then return end
	local align, palign

	if (not direction) then
		if (not BarMath_CurrentBarOrder) then
			direction = -1
		else
			direction = BarMath_CurrentBarOrder
		end
	end
	if (direction <= 0) then
		direction = -1
		align, palign = "TOP","BOTTOM"
	elseif (direction >= 1) then
		direction = 1
	end

	BarMath_CurrentBarOrder = direction

	BarMath_Msg("Reordering Bars...","debug")

	local offset = bm.BarHeight * direction
	local titleoffset = 1

	if (BarMath_GetCharVar("ShowParentBar") == 0) then
		BarMathTitle:Hide()
		titleoffset = 0
	else
		BarMathTitle:Show()
	end

	local visible = {}
	local vx = 1

	for x=1,#(BarMath_BarList) do
		local cbar = BarMath_BarList[x]
		local a = _G["BarMath"..cbar["Name"].."Bar"]

		if (BarMath_GetCharVar("Bars",cbar["Name"].."Bar","Display") == 1) and (a:IsVisible()) then
			visible[vx] = x
			vx = vx+1
		end
	end

	BarMath_Msg("Visible count: "..#visible..", VX: "..vx, "debug")

	for bar=1,#(visible) do
		local cbar = BarMath_BarList[visible[bar]]
		align = "BOTTOM"
		palign = "TOP"
		if (bar == 1) then
			finaloffset = 1+titleoffset
		else
			finaloffset = bar+titleoffset
		end
		finaloffset = finaloffset*offset
		BarMath_Msg("For Bar:"..cbar["Name"]..", Offset:"..offset..", FinalOffset:"..finaloffset,"debug")
		cf = _G["BarMath"..cbar["Name"]]
		cf:Show()
		cf:ClearAllPoints()
		cf:SetPoint(align, BarMath_ParentFrame, palign, 0, finaloffset)
		cf:SetAlpha(1)
		cf:SetFrameStrata("HIGH")
		local a = _G[BarMath_BarList[bar]["Update"].."()"]
	end

	BarMath_ParentBar_SetPos()
	BarMath_Msg("Finished Reordering Bars.","debug")
end

function BarMath_Update_All()
	--BarMath_Msg("Updating All...")
	for x=1,#BarMath_BarList do
		local update = _G[BarMath_BarList[x]["Update"]]
		update()
		BarMath_Bar_ShowHide(BarMath_BarList[x]["Name"])
	--	BarMath_Msg("\""..BarMath_BarList[x]["Update"].."\" Ran, "..BarMath_BarList[x]["Name"].." Bar updated!")
	end
	BarMath_Minimap_OnOff()
	BarMath_ReorderBars(-1)
end

--== Major Functions ==--

-- Add Event
function BarMath_AddEvent(Event, Function)
	if (not Event) then
		BarMath_Msg(BARMATH_DEVERROR_EVENT_NOEVENT,"DevError")
		return
	end
	if (not Function) then
		BarMath_Msg(BARMATH_DEVERROR_EVENT_NOFUNCTION..Event.."\".","DevError")
		return
	end
	--BarMath_Msg("Registering Event \""..Event.."\" with function \""..tostring(Function).."\".")
	local event = tostring(Event)
	if (not BarMath_EventList[event]) then
		BarMath_EventList[event] = {}
		--BarMath_Msg("Event not in List. Generating...")
	end
	local c_count = #BarMath_EventList[Event]
	BarMath_EventList[event][(c_count+1)] = Function
	BarMath_ParentFrame:RegisterEvent(event)
end

-- Add a Bar to BarMath's Database
function BarMath_AddBar(BarTitle, Update_Func, Defaults, parent, Info_Delete_Func, BarTemplate)
	if (bm.InCombat==true) then return end
	if (BarTitle == nil) then
		--BarMath_Msg(BARMATH_DEVERROR_ADDBAR_TITLENOTSET, "DevError")
		return
	end
	if (not Update_Func) then
		--BarMath_Msg(BARMATH_DEVERROR_ADDBAR_NOUPDATEFUNC_PRETITLE..BarTitle..BARMATH_DEVERROR_ADDBAR_NOUPDATEFUNC_POSTTITLE, "DevError")
		return
	end
	if (not Defaults) then
		--BarMath_Msg("Default for "..BarTitle.." not specified. Using Generics.")
		Defaults = BarMath_Defaults["Bars"]
	end
	if (not parent) then
		parent = ""
	end
	if (not BarTemplate) then
		BarTemplate = ""
	end

	BarMath_BuildDB()

	if (not BarMath_GetCharVar("Bars",BarTitle.."Bar")) or (_G["BarMath_"..BarTitle.."_Version"] and (_G["BarMath_"..BarTitle.."_Version"] < _G["BarMath_"..BarTitle.."_DBRewrite_Version"])) then
		BarMath_SetCharVar(Defaults, "Bars", BarTitle.."Bar")
	end

	local a = BarMath_GetCharVar("Bars",BarTitle.."Bar","Version")
	if (a and (a < _G["BarMath_"..BarTitle.."_Version"])) then
		--BarMath_AddonOptionUpdate(BarTitle, Defaults)
	end

	if (not _G["BarMath"..BarTitle.."Bar"]) then
		BarMath_MakeBar(BarTitle, parent, Update_Func, BarTemplate)
	end

	if (Info_Delete_Func ~= nil) then
		BarMath_BarInfoDelete[BarTitle] = Info_Delete_Func
	end

	local a = _G[Update_Func.."()"]
	BarMath_ParentBar_SetPos()
end

-- Call this to MAKE the bars. (Replaces XML data!)
function BarMath_MakeBar(bartitle, parent, Update_Func, BarTemplate, Info_Delete_Func)
	if (bm.InCombat==true) then return end
	if ((not BarTemplate) or (BarTemplate == "")) then
		BarTemplate = "BarMath_Bar_Template"
	end
	--BarMath_Msg("Making Bar "..bartitle.."...")
	local offset = bm.BarHeight -- Should always be equal to the template's height!
	local trueparent, y

	--BarMath_Msg("BarCount: "..BarMath_Barcount)

	if (BarMath_Barcount == 0) then
		trueparent = "BarMath_ParentFrame"
		y = -bm.BarHeight
	else
		if (not parent or parent=="") then
			parent = BarMath_BarList[BarMath_Barcount]["Name"]
		end
		trueparent = "BarMath"..parent
		y = -offset
	end

	--BarMath_Msg(bartitle.."'s Parent is "..trueparent..", and it's offset is "..y)

	local f = CreateFrame("Frame", "BarMath"..bartitle, getglobal("BarMath_ParentFrame"), BarTemplate)
	f:ClearAllPoints()
	f:SetPoint("TOP", trueparent, "TOP", 0, y)
	f:SetFrameStrata("HIGH")
	getglobal("BarMath"..bartitle.."Bar"):SetScript("OnEnter", function() BarMath_Bar_ShowHide(bartitle, "true") end)
	getglobal("BarMath"..bartitle.."Bar"):SetScript("OnLeave", function() BarMath_Bar_ShowHide(bartitle) end)
	f:Show()
	--BarMath_Msg(bartitle.." isvisible: "..f:IsVisible())

	BarMath_Barcount = BarMath_Barcount + 1

	BarMath_BarList[BarMath_Barcount] = {
		["Name"] = bartitle,
		["Parent"] = trueparent,
		["Update"] = Update_Func,
		["InfoDelete"] = Info_Delete_Func,
	}

	BarMath_HaveLoaded["Bars"][bartitle] = 1
end

--== Database Functions ==--

function BarMath_BuildDB()
	if (not BarMath_Characters) then
		BarMath_Characters = {}
	end
	if (not BarMath_Characters[BarMath_Realm]) then
		BarMath_Characters[BarMath_Realm] = {}
	end
	if (not BarMath_Characters[BarMath_Realm][BarMath_Faction]) then
		BarMath_Characters[BarMath_Realm][BarMath_Faction] = {}
	end
	if (not BarMath_Characters[BarMath_Realm][BarMath_Faction][BarMath_PlayerName]) then
		BarMath_Characters[BarMath_Realm][BarMath_Faction][BarMath_PlayerName] = BarMath_Defaults["Options"]
		BarMath_NewPlayer = true
	end

	local CharDB = BarMath_Characters[BarMath_Realm][BarMath_Faction][BarMath_PlayerName]

	if (not CharDB["Version"]) or (CharDB["Version"] < bm.Version) then
		BarMath_CharDatabaseUpdate()
	end

	if (not BarMath_AddonData) then
		BarMath_AddonData = {}
		BarMath_NewPlayer = true
	end
end

function BarMath_BuildAddonDB(addon, database)
	if ((not addon) or (type(addon) == "table")) then
		BarMath_Msg(BARMATH_DEVERROR_INFODB_GENERATIONFAILED, "DevError")
		return
	end

	BarMath_BuildDB()
	if (not BarMath_AddonData[addon]) then
		BarMath_AddonData[addon] = {}
	end

	for k,v in pairs(database) do
		if (type(v) == "table") then
			if (not BarMath_AddonData[addon][k]) then
				BarMath_AddonData[addon][k] = {}
			end
			for k2,v2 in pairs(database[k]) do
				if (type(v2) == "table") then
					if (not BarMath_AddonData[addon][k][k2]) then
						BarMath_AddonData[addon][k][k2] = {}
					end
					for k3,v3 in pairs(database[k][k2]) do
						if (type(v3) == "table") then
							if (not BarMath_AddonData[addon][k][k2][k3]) then
								BarMath_AddonData[addon][k][k2][k3] = {}
							end
							for k4,v4 in pairs(database[k][k2][k3]) do
								if (type(v4) == "table") then
									if (not BarMath_AddonData[addon][k][k2][k3][k4]) then
										BarMath_AddonData[addon][k][k2][k3][k4] = {}
									end
									for k5,v5 in pairs(database[k][k2][k3][k4]) do
										if (type(v5) == "table") then
											if (not BarMath_AddonData[addon][k][k2][k3][k4][k5]) then
												BarMath_AddonData[addon][k][k2][k3][k4][k5] = {}
											end
											for k6,v6 in pairs(database[k][k2][k3][k4][k5]) do
												if (type(v6) == "table") then
													if (not BarMath_AddonData[addon][k][k2][k3][k4][k6]) then
														BarMath_AddonData[addon][k][k2][k3][k4][k6] = {}
													end
													for k7,v7 in pairs(database[k][k2][k3][k4][k6]) do
														if (type(v7) == "table") then
															BarMath_Msg(BARMATH_DEVERROR_INFODB_TOOMANYLEVELS,"DevError")
														elseif ((not BarMath_AddonData[addon][k][k2][k3][k4][k5][k6][k7])and(k7~=nil and v7~=nil)) then
															BarMath_AddonData[addon][k][k2][k3][k4][k5][k6][k7] = v7
														end
													end
												elseif ((not BarMath_AddonData[addon][k][k2][k3][k4][k5][k6])and(k6~=nil and v6~=nil)) then
													BarMath_AddonData[addon][k][k2][k3][k4][k5][k6] = v6
												end
											end
										elseif ((not BarMath_AddonData[addon][k][k2][k3][k4][k5])and(k5~=nil and v5~=nil)) then
											BarMath_AddonData[addon][k][k2][k3][k4][k5] = v5
										end
									end
								elseif ((not BarMath_AddonData[addon][k][k2][k3][k4])and(k4~=nil and v4~=nil)) then
									BarMath_AddonData[addon][k][k2][k3][k4] = v4
								end
							end
						elseif ((not BarMath_AddonData[addon][k][k2][k3])and(k3~=nil and v3~=nil)) then
							BarMath_AddonData[addon][k][k2][k3] = v3
						end
					end
				elseif ((not BarMath_AddonData[addon][k][k2])and(k2~=nil and v2~=nil)) then
					BarMath_AddonData[addon][k][k2] = v2
				end
			end
		elseif ((not BarMath_AddonData[addon][k])and(k~=nil and v~=nil)) then
			BarMath_AddonData[addon][k] = v
		end
	end
end

function BarMath_CharDatabaseUpdate()
	local temp = BarMath_Defaults["Options"]
	
	local CharDB = BarMath_Characters[BarMath_Realm][BarMath_Faction][BarMath_PlayerName]
	for k,v in pairs(CharDB) do
		if (type(temp[k])=="table") and (type(CharDB[k]) == "table") then
			for k2,v2 in pairs(CharDB[k]) do
				if (type(temp[k][k2])=="table") and (type(CharDB[k][k2]) == "table") then
					for k3,v3 in pairs(CharDB[k][k2]) do
						if (type(temp[k][k2][k3])=="table") and (type(CharDB[k][k2][k3]) == "table") then
							for k4,v4 in pairs(CharDB[k][k2][k3]) do
								if (type(temp[k][k2][k3][k4])=="table") and (type(CharDB[k][k2][k3][k4]) == "table") then
									for k5,v5 in pairs(CharDB[k][k2][k3][k5]) do
										if ((CharDB[k][k2][k3][k4][k5]) and (v5 ~= nil)) then
											temp[k][k2][k3][k4][k5] = v5
										end
									end
								elseif ((CharDB[k][k2][k3][k4]) and (v4 ~= nil)) then
									temp[k][k2][k3][k4] = v4
								end
							end
						elseif ((CharDB[k][k2][k3]) and (v3 ~= nil)) then
							temp[k][k2][k3] = v3
						end
					end
				elseif ((CharDB[k][k2]) and (v2 ~= nil)) then
					temp[k][k2] = v2
				end
			end
		elseif ((CharDB[k]) and (v ~= nil)) then
			temp[k] = v
		end
	end
	temp["Version"] = bm.Version

	wipe(BarMath_Characters[BarMath_Realm][BarMath_Faction][BarMath_PlayerName]) -- Just in case, erase all previous settings.
	BarMath_Characters[BarMath_Realm][BarMath_Faction][BarMath_PlayerName] = temp
end

function BarMath_SetCharVar(value, ...)
	local v1, v2, v3, v4, v5, v6, v7 = ...

	if (v7 and v7 ~= "") then
		BarMath_Characters[BarMath_Realm][BarMath_Faction][BarMath_PlayerName][v1][v2][v3][v4][v5][v6][v7] = value
		return
	elseif (v6 and v6 ~= "") then
		BarMath_Characters[BarMath_Realm][BarMath_Faction][BarMath_PlayerName][v1][v2][v3][v4][v5][v6] = value
		return
	elseif (v5 and v5 ~= "") then
		BarMath_Characters[BarMath_Realm][BarMath_Faction][BarMath_PlayerName][v1][v2][v3][v4][v5] = value
		return
	elseif (v4 and v4 ~= "") then
		BarMath_Characters[BarMath_Realm][BarMath_Faction][BarMath_PlayerName][v1][v2][v3][v4] = value
		return
	elseif (v3 and v3 ~= "") then
		BarMath_Characters[BarMath_Realm][BarMath_Faction][BarMath_PlayerName][v1][v2][v3] = value
		return
	elseif (v2 and v2 ~= "") then
		BarMath_Characters[BarMath_Realm][BarMath_Faction][BarMath_PlayerName][v1][v2] = value
		return
	elseif (v1 and v1 ~= "") then
		BarMath_Characters[BarMath_Realm][BarMath_Faction][BarMath_PlayerName][v1] = value
		return
	end
end

function BarMath_GetCharVar(...)
	if (not BarMath_Characters) then
		BarMath_BuildDB()
	end
	local db = BarMath_Characters[BarMath_Realm][BarMath_Faction][BarMath_PlayerName]
	local v1, v2, v3, v4, v5, v6, v7 = ...
	local value

	if (v7 and v7 ~= "") then -- and (db[v1] and db[v1][v2] and db[v1][v2][v3] and db[v1][v2][v3][v4] and db[v1][v2][v3][v4][v5] and db[v1][v2][v3][v4][v5][v6] and db[v1][v2][v3][v4][v5][v6][v7]) then
		value = db[v1][v2][v3][v4][v5][v6][v7]
	elseif (v6 and v6 ~= "") then -- and (db[v1] and db[v1][v2] and db[v1][v2][v3] and db[v1][v2][v3][v4] and db[v1][v2][v3][v4][v5] and db[v1][v2][v3][v4][v5][v6]) then
		value = db[v1][v2][v3][v4][v5][v6]
	elseif (v5 and v5 ~= "") then -- and (db[v1] and db[v1][v2] and db[v1][v2][v3] and db[v1][v2][v3][v4] and db[v1][v2][v3][v4][v5]) then
		value = db[v1][v2][v3][v4][v5]
	elseif (v4 and v4 ~= "") then -- and (db[v1] and db[v1][v2] and db[v1][v2][v3] and db[v1][v2][v3][v4]) then
		value = db[v1][v2][v3][v4]
	elseif (v3 and v3 ~= "") then -- and (db[v1] and db[v1][v2] and db[v1][v2][v3]) then
		value = db[v1][v2][v3]
	elseif (v2 and v2 ~= "") then -- and (db[v1] and db[v1][v2]) then
		value = db[v1][v2]
	elseif (v1 and v1 ~= "") and (db[v1]) then
		value = db[v1]
	end

	return value
end

function BarMath_SetAddonVar(addon, value, ...)
	local v1, v2, v3, v4, v5, v6, v7 = ...

	if (v7 and v7 ~= "") then
		BarMath_AddonData[addon][v1][v2][v3][v4][v5][v6][v7] = value
		return
	elseif (v6 and v6 ~= "") then
		BarMath_AddonData[addon][v1][v2][v3][v4][v5][v6] = value
		return
	elseif (v5 and v5 ~= "") then
		BarMath_AddonData[addon][v1][v2][v3][v4][v5] = value
		return
	elseif (v4 and v4 ~= "") then
		BarMath_AddonData[addon][v1][v2][v3][v4] = value
		return
	elseif (v3 and v3 ~= "") then
		BarMath_AddonData[addon][v1][v2][v3] = value
		return
	elseif (v2 and v2 ~= "") then
		BarMath_AddonData[addon][v1][v2] = value
		return
	elseif (v1 and v1 ~= "") and (db[v1]) then
		BarMath_AddonData[addon][v1] = value
		return
	else
		return
	end
end

function BarMath_GetAddonVar(addon, ...)
	local db = BarMath_AddonData[addon]
	local v1, v2, v3, v4, v5, v6, v7 = ...

	if (v7 and v7 ~= "") and (db[v1] and db[v1][v2] and db[v1][v2][v3] and db[v1][v2][v3][v4] and db[v1][v2][v3][v4][v5] and db[v1][v2][v3][v4][v5][v6] and db[v1][v2][v3][v4][v5][v6][v7]) then
		return db[v1][v2][v3][v4][v5][v6][v7]
	elseif (v6 and v6 ~= "") and (db[v1] and db[v1][v2] and db[v1][v2][v3] and db[v1][v2][v3][v4] and db[v1][v2][v3][v4][v5] and db[v1][v2][v3][v4][v5][v6]) then
		return db[v1][v2][v3][v4][v5][v6]
	elseif (v5 and v5 ~= "") and (db[v1] and db[v1][v2] and db[v1][v2][v3] and db[v1][v2][v3][v4] and db[v1][v2][v3][v4][v5]) then
		return db[v1][v2][v3][v4][v5]
	elseif (v4 and v4 ~= "") and (db[v1] and db[v1][v2] and db[v1][v2][v3] and db[v1][v2][v3][v4]) then
		return db[v1][v2][v3][v4]
	elseif (v3 and v3 ~= "") and (db[v1] and db[v1][v2] and db[v1][v2][v3]) then
		return db[v1][v2][v3]
	elseif (v2 and v2 ~= "") and (db[v1] and db[v1][v2]) then
		return db[v1][v2]
	elseif (v1 and v1 ~= "") and (db[v1]) then
		return db[v1]
	else
		return
	end
end

--== UI functions ==--

function BarMath_Generate_Options(BarTitle,displayname,pulldown,tabs)
	if (not BarTitle) then
		BarMath_Msg(BARMATH_DEVERROR_NOBARTITLE,"DevError")
	elseif (BarMath_Option_Addons[BarTitle]) then
		BarMath_Msg(BARMATH_DEVERROR_ADDONOPTIONSEXIST,"DevError")
		return
	end

	BarMath_Option_Addons[BarTitle] = {
		["Title"] = displayname,
		["Pulldown"] = pulldown,
		["Tabs"] = {},
	}
	
	if (#tabs > 0) then
		BarMath_Option_Addons[BarTitle]["Tabs"] = tabs
	end
end

function BarMath_Minimap_OnOff()
	if BarMath_GetCharVar("ShowMinimapButton") == 0 then
		BarMathMinimapButton:Hide()
	else
		BarMathMinimapButton:Show()
	end
end

--== Utility Functions ==--

-- Command Line Handler
function BarMath_commandline(cmd)
	cmd = strlower(cmd)
	if (not BarMath_Characters) then
		BarMath_BuildDB()
	end

	if (cmd == "") then
		BarMath_Msg("bleh")
	elseif (cmd == BARMATH_CMD_COMMA_ON) or (cmd == BARMATH_CMD_COMMAS_ON) then
		BarMath_SetCharVar(1, "UseCommas")
		BarMath_Update_All()
	elseif (cmd == BARMATH_CMD_COMMA_OFF) or (cmd == BARMATH_CMD_COMMAS_OFF) then
		BarMath_SetCharVar(0, "UseCommas")
		BarMath_Update_All()
	elseif (cmd == BARMATH_CMD_PARENTBAR_ON) then
		BarMath_SetCharVar(1,"ShowParentBar")
		BarMath_Update_All()
	elseif (cmd == BARMATH_CMD_PARENTBAR_OFF) then
		BarMath_SetCharVar(0,"ShowParentBar")
		BarMath_Update_All()
	else
		BarMath_Msg(BARMATH_ERROR_UNKNOWNCOMMAND,"Error")
	end
end

-- Message output function.
function BarMath_Msg(msg, channel)
	if (msg == nil) then
		return
	end
	-- User Error Message Channel
	if (channel == "Error") then
		DEFAULT_CHAT_FRAME:AddMessage(RED_FONT_COLOR_CODE..L["BarMath Title"].." "..BARMATH_TXT_ERROR..": |r"..msg)

	-- Developer Error Message Channel
	elseif (channel == "DevError") then
		DEFAULT_CHAT_FRAME:AddMessage(RED_FONT_COLOR_CODE..L["BarMath Title"].." "..BARMATH_TXT_DEVERROR..": |r"..msg)

	-- Debug Message Channel
	elseif (channel == "debug") or (channel == "Debug") then
		if (BarMath_Debug == 1) then
			DEFAULT_CHAT_FRAME:AddMessage(BarMath_Color_02..L["BarMath Title"].." "..BARMATH_TXT_DEBUG..": |r"..tostring(msg))
		end

	-- All Other Messages
	else
		DEFAULT_CHAT_FRAME:AddMessage(BarMath_Color_01..L["BarMath Title"]..": |r"..tostring(msg))
	end
end

-- Timer function
function BarMath_Timer(name,targettime,functiontodo)
	local framename = "BarMathTimeFrame"..name
	if getglobal(framename) and (getglobal(framename):GetScript("OnUpdate") ~= nil) then
		return
	end

	local frame = CreateFrame("Frame", framename)
	
	local totalElapsed = 0.0
	local tickcount = 0

	local function BarMathTimer_onUpdate(self, elapsed)
		totalElapsed = totalElapsed + elapsed
		if (totalElapsed < 1) then return end
		totalElapsed = totalElapsed - floor(totalElapsed)
		tickcount = tickcount + 1

		if (tickcount == targettime) then
			functiontodo()
			frame:SetScript("OnUpdate", nil)
			return
		end
		-- Enable for timing debugging
		-- ChatFrame1:AddMessage("tick... "..tickcount)
	end
	frame:SetScript("OnUpdate", BarMathTimer_onUpdate)
	frame:Show()
end

-- Rounding function
function BarMath_Round(num, decimal, mode)
	if (not decimal) then
		decimal = BarMath_GetCharVar("Decimals")
	end
	if (not mode) then
		mode = BarMath_GetCharVar("Rounding")
	end
	local idp = tonumber(decimal)
	local mult = 10^(idp or 0)
	--BarMath_Msg('Rounding "'..tostring(num)..'" '..tostring(mode)..', to the nearest "'..tostring(idp)..'" spot.',"debug")
	if (mode == BARMATH_ROUNDING_UP) then
		result = math.ceil(num * mult) / mult
	elseif (mode == BARMATH_ROUNDING_STANDARD) then
		result = math.floor(num * mult + 0.5) / mult
	else
		result = math.floor(num * mult) / mult
	end
	BarMath_Msg('Rounding Result: "'..result..'"',"debug")
	return result
end

-- Add Comma function
function BarMath_AddCommas(input)
	if (BarMath_GetCharVar("UseCommas") == 0) then
		return input
	end
	if (type(input) ~= "number") then
		return input
	end

	--BarMath_Msg("Add Comma Input:"..tostring(input))

	local l = strlen(input)
	local d = strfind(input, "%.")
	local newstr, ext

	--BarMath_Msg("Input Len:"..tostring(l)..", Dot:"..tostring(d))

	if (d) and (d ~= 1) then
		ext = strsub(input, d)
		input = strsub(input, 1,d-1)
		l = d-1
	end

	-- BarMath_Msg("Input Len, with D:"..tostring(l))

	if l < 4 then
		if ext then
			return input..ext
		else
			return input
		end
	end

	--BarMath_Msg("Starting Cycle with input:"..tostring(input).."...")

	for i=l, 1, -3 do
		local x = i-2
		if x < 1 then x = 1 end
		--BarMath_Msg("Cycle i:"..i.." to "..x)
		local sub = strsub(input, x, i)
		--BarMath_Msg("Sub:"..tostring(sub))
		
		if (newstr == nil) then
			newstr = sub
			if ext then
				newstr = newstr..ext
			end
		else
			newstr = sub..","..newstr
		end
	end

	--BarMath_Msg("Add Comma Output:"..tostring(newstr))

	return newstr
end

-- Set Text function. Determines what to set each text to.
function BarMath_SetBarText(Text, Bar, Slot)
	local a = _G["BarMath"..Bar.."BarDisplay"..Slot.."Text"]
	a:SetText(Text)
end

-- Master Show/Hide function. This will determine if a Bar, and all it's elements should be seen or not.
function BarMath_Bar_ShowHide(barname, enter)
	if (bm.InCombat==true) then return end
	if (not enter) then
		enter = "false"
	end

	if (BarMath_GetCharVar("Bars",barname.."Bar","Display") == 1) then
		_G["BarMath"..barname]:Show()
	else
		_G["BarMath"..barname]:Hide()
	end

	-- If the bar is hidden, don't bother show/hiding the rest.
	if (getglobal("BarMath"..barname):IsVisible()) then
		for x=1,bm.DisplayCount do
			BarMath_ShowHideDisplay(barname, x, enter)
		end
	end
	BarMath_ReorderBars(BarMath_CurrentBarOrder)
end

-- show/Hide Display
function BarMath_ShowHideDisplay(Bar, Slot, enter)
	if (bm.InCombat==true) then return end
	if (not enter) then
		enter = "false"
	end
	local a = _G["BarMath"..Bar.."BarDisplay"..Slot.."Text"]

	if ((BarMath_GetCharVar("Bars",Bar.."Bar","DisplayMethod") == 1) or (enter == "true")) then
		if (a:GetText() ~= "nil") and (BarMath_GetCharVar("Bars",Bar.."Bar","Displays","Display"..Slot.."OnOff") == 1) then
			a:Show()
		else
			a:Hide()
		end
	else
		a:Hide()
	end
end

-- Sort tables by Keys
function BarMath_PairsByKeys (t, f)
	local a = {}
		for n in pairs(t) do table.insert(a, n) end
		table.sort(a, f)
		local i = 0      -- iterator variable
		local iter = function ()   -- iterator function
			i = i + 1
			if a[i] == nil then return nil
			else return a[i], t[a[i]]
			end
		end
	return iter
end