--[[
	BarMath
	Primary Localization File
	Version <%version%>
	
	Revision: $Id: Localization.lua 3 2012-12-23 11:45:34 PST Kjasi $
]]

local bm = _G.BarMath
if (not bm) then
	print(RED_FONT_COLOR_CODE.."Localization is unable to find BarMath's Global."..FONT_COLOR_CODE_CLOSE)
	return
end
bm.Localize = {}
local L = bm.Localize

-- Title
L["BarMath Title"] = "BarMath"
BARMATH_TITLE = L["BarMath Title"]	-- Here until we can get it all cleaned up...