--[[
	BarMath
	English Localization
	Version <%version%>
	
	Revision: $Id: enUS.lua 7 2012-12-23 11:44:51 PST Kjasi $
]]

-- Basic Text
BARMATH_TXT_DEBUG = "Debug";
BARMATH_TXT_ERROR = "Error";
BARMATH_TXT_DEVERROR = "Developer Error";
BARMATH_TXT_NONE = "None";
BARMATH_TXT_HORDE = "Horde";
BARMATH_TXT_ALLIANCE = "Alliance";

-- Messages
BARMATH_MSG_LOADED = BARMATH_TITLE.." v%s has loaded successfully!";	-- Passed Vars: Version Number.

-- Bar Text
BARMATH_BAR_TITLE = BARMATH_TITLE..", v%s"	-- Passed Vars: Version Number.

-- Options
BARMATH_ROUNDING_STANDARD = "Standard Rounding";
BARMATH_ROUNDING_UP = "Always Round Up";
BARMATH_ROUNDING_DOWN = "Always Round Down";
BARMATH_BARTEXT_SHORT = "Short";
BARMATH_BARTEXT_LONG = "Long";

-- Addon Developer Errors
BARMATH_DEVERROR_NOBARTITLE = "No BarTitle Specified. Aborting."
BARMATH_DEVERROR_ADDONOPTIONSEXIST = "Addon Options already exist. Aborting.";
BARMATH_DEVERROR_TOOMANYTABS = "Your addon has too many tabs. Please reduce your tabs. You are allowed: ";
BARMATH_DEVERROR_NOVALUEFOUND = "The specified variable could not be returned. Either you're looking in the wrong place, or this variable does not exist.";
BARMATH_DEVERROR_INFODB_GENERATIONFAILED = "Addon Not Specified. Info Generation failed.";
BARMATH_DEVERROR_INFODB_TOOMANYLEVELS = "BarMath currently does not support over 7 levels of the shared database."; -- Note: If the supported number of levels is increased, this line will need re-translation.
BARMATH_DEVERROR_ADDBAR_TITLENOTSET = "BarTitle Not Set.";
BARMATH_DEVERROR_ADDBAR_NOUPDATEFUNC_PRETITLE = "Update Function for ";
BARMATH_DEVERROR_ADDBAR_NOUPDATEFUNC_POSTTITLE = "'s Bar Not Set.";
BARMATH_DEVERROR_KILLBAR_NOBAR = "Bar not specified.";
BARMATH_DEVERROR_EVENT_NOEVENT = "Event Not Specified.";
BARMATH_DEVERROR_EVENT_NOFUNCTION = "Function not found. Could not add event \"";
BARMATH_DEVERROR_CMDLINE_BARNOTFOUND = "Specified Bar not found. Did you use proper Capitalization?";

--Command-line Commands
BARMATH_CMD_HELP = "help";
BARMATH_CMD_COMMA_ON = "comma on";
BARMATH_CMD_COMMA_OFF = "comma off";
BARMATH_CMD_COMMAS_ON = "commas on";
BARMATH_CMD_COMMAS_OFF = "commas off";
BARMATH_CMD_PARENTBAR_ON = "parentbar on";
BARMATH_CMD_PARENTBAR_OFF = "parentbar off";

-- Errors
BARMATH_ERROR_UNKNOWNCOMMAND = "Unknown command.";

--== Bar Text ==--
BARMATH_BARTXT_BARS = "Bars: ";
BARMATH_BARTXT_BARSFILLED = "Bars Filled: ";
BARMATH_BARTXT_TOGO = " To Go";
BARMATH_BARTXT_PERBAR = "Per Bar: ";
BARMATH_BARTXT_PERCENT = "Percent: ";
BARMATH_BARTXT_PERCENT_EARNED = "Percent Earned: ";
BARMATH_BARTXT_P_NEEDED = "% Needed: ";

--== Options Window ==--
BARMATH_OPTIONS_SHOWMINIMAP = "Show Minimap Icon";
BARMATH_OPTIONS_SHOWPARENTBAR = "Show Parent Bar";
BARMATH_OPTIONS_SHOWALLSERVERS = "Show All Servers";
BARMATH_OPTIONS_SHOWBOTHFACTIONS = "Show Both Factions";
BARMATH_OPTIONS_ALLOWSNAPTO = "Allow Snap-To Zones";
BARMATH_OPTIONS_CHARACTERLIST_TITLE = "Character List";
BARMATH_OPTIONS_USECOMMAS = "Use Commas";

-- General Tab Basic Options
BARMATH_OPTIONS_GENERAL_TAB = "General Options";		-- Tab's Title
BARMATH_OPTIONS_GENERAL_BARTEXT = "Bar Text Options";
BARMATH_OPTIONS_GENERAL_SHOWBARTXT_CHECKBOX = "Always Show Text";

-- Display Options
BARMATH_DISPLAYOPTION_BAR_FILLED = "Amount of Bars Filled";
BARMATH_DISPLAYOPTION_BAR_TOFILL = "Bars needed to Level";
BARMATH_DISPLAYOPTION_CURRMAX = "Current / Maximum";
BARMATH_DISPLAYOPTION_PERC_FILLED = "Percentage Filled";
BARMATH_DISPLAYOPTION_PERC_TOFILL = "Percentage Needed to Level";

-- Tooltips
BARMATH_MINIMAP_TOOLTIP = "Opens and Closes the BarMath Interface.\n\nClick and drag to move the icon anywhere on the screen.";