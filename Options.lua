--[[
	BarMath
	Options Window File
	Version <%version%>
	
	Revision: $Id: Options.lua 8 2012-12-23 11:46:52 PST Kjasi $
]]

local bm = _G.BarMath
if (not bm) then
	print(RED_FONT_COLOR_CODE.."Options is unable to find BarMath's Global.|r")
	return
end

local L = bm.Localize
if (not L) then
	print(RED_FONT_COLOR_CODE.."Options is unable to find BarMath's Localization.|r")
	return
end

local Pre, Post, Text = "BarMath_Options", "CheckBox", "Text"
local BarMathOptions_AddonBtnList = {}
local BarMath_CurrentToon
local BarMath_CurrentButton = "BarMath_OptionsAddonListingBtn1"
local BarMath_CurrentButtonTab = 1

local BarMath_TotalChars, BarMath_MaxTabs = 40, 12

local VarsBackup = {
	["CheckBoxs"] = {},
}

-- Global Checkbox table.
BarMath_Option_CheckBoxes = {
	["ShowMinimap"] = {
		["Variable"] = "ShowMinimapButton",
		["Text"] = BARMATH_OPTIONS_SHOWMINIMAP,
	},
	["ShowParentBar"] = {
		["Variable"] = "ShowParentBar",
		["Text"] = BARMATH_OPTIONS_SHOWPARENTBAR,
	},
	["CBServer"] = {
		["Variable"] = "CharListShowOtherServers",
		["Text"] = BARMATH_OPTIONS_SHOWALLSERVERS,
	},
	["CBFactions"] = {
		["Variable"] = "CharListShowOtherFactions",
		["Text"] = BARMATH_OPTIONS_SHOWBOTHFACTIONS,
	},
	["AllowSnapTo"] = {
		["Variable"] = "AllowSnapToZones",
		["Text"] = BARMATH_OPTIONS_ALLOWSNAPTO,
	},
	["UseCommas"] = {
		["Variable"] = "UseCommas",
		["Text"] = BARMATH_OPTIONS_USECOMMAS,
	},
}

BarMath_Option_PullDown_RawBarsPercent = {
	"BARMATH_DISPLAYAS_RAWNUM",
	"BARMATH_DISPLAYAS_BARS",
	"BARMATH_DISPLAYAS_PERCENT",
}

BarMath_Option_Addons = {}
BarMath_Option_Addons["Character"] = {
	["Title"] = BARMATH_OPTIONS_CHARACTERLIST_TITLE,
	["Tabs"] = {}
}

-- Generate all text, and other options when loading the option frame.
function BarMath_Options_Load()
	BarMath_OptionsTitle:SetText(HIGHLIGHT_FONT_COLOR_CODE..format(BARMATH_BAR_TITLE,bm.Version)..FONT_COLOR_CODE_CLOSE)

	for box,v in pairs(BarMath_Option_CheckBoxes) do
		_G[Pre..box.."Text"]:SetText(NORMAL_FONT_COLOR_CODE..BarMath_Option_CheckBoxes[box]["Text"]..FONT_COLOR_CODE_CLOSE)
	end

	BarMath_Options_Update()
end

-- Used to generate the Addon list
function BarMath_Options_PopulateList()
	BarMathOptions_AddonBtnList = {}

	local btnname = "BarMath_OptionsAddonListingBtn"
	local btns = 1

	if (not _G[btnname..tostring(btns)]) then
		local parent, xoffset, yoffset = BarMath_OptionsAddonListing, 0, 0
		local f = CreateFrame("Button",btnname..tostring(btns),parent,"BarMathOptions_AddonListBtn")
		f:SetPoint("TOPLEFT",parent,"TOPLEFT", xoffset, yoffset)
	end
	_G[btnname..tostring(btns)]:SetNormalFontObject(GameFontNormal)
	_G[btnname..tostring(btns)]:SetHighlightFontObject(GameFontHighlight)
	_G[btnname..tostring(btns)]:SetText(BarMath_Option_Addons["Character"]["Title"])

	BarMathOptions_AddonBtnList[btnname..tostring(btns)] = "Character"

	btns = btns + 1

	local theseaddons = BarMath_Option_Addons

	for addon,addonv in BarMath_PairsByKeys(theseaddons) do
		BarMath_Msg("Setting Options for: "..tostring(theseaddons[addon]["Title"]),"debug")
		if (addon~="Character") then
			local f
			local parent, xoffset, yoffset = getglobal(btnname..(btns - 1)), 0, -18
			if (not getglobal(btnname..tostring(btns))) then
				f = CreateFrame("Button",btnname..tostring(btns),parent,"BarMathOptions_AddonListBtn")
			else
				f = getglobal(btnname..tostring(btns))
			end
			f:SetPoint("TOPLEFT",parent,"TOPLEFT", xoffset, yoffset)

			_G[btnname..tostring(btns)]:SetText(addonv["Title"])
			BarMathOptions_AddonBtnList[btnname..tostring(btns)] = addon

			btns = btns + 1
		end
	end
end

-- Update the options for any checkbox
function BarMath_Options_CheckboxOnOff(checkbox, value)
	if (value == nil) then
		value = 0
	end

	for k,v in pairs(BarMath_Option_CheckBoxes) do
		if (checkbox == Pre..k) then
			BarMath_SetCharVar(tonumber(value),BarMath_Option_CheckBoxes[k]["Variable"])
			break
		end
	end

	BarMath_Options_Update()
end

-- Used to call all the update functions of BarMath
function BarMath_Options_Update()
	BarMath_Update_All()
	if (BarMath_Options_CharacterList:IsVisible()) then
		BarMath_CharacterList_PopulateList()
	end
end

-- Things to do when showing the Options window.
function BarMath_Options_OnShow()
	-- Set initial Checkbox Options
	for bar,var in pairs(BarMath_Option_CheckBoxes) do
		local vars
		if (type(BarMath_Option_CheckBoxes[bar]["Variable"]) == "table") then
			for g=1,#var do
				if g==1 then
					vars = BarMath_Option_CheckBoxes[bar]["Variable"][g]
				else
					vars = vars..","..BarMath_Option_CheckBoxes[bar]["Variable"][g]
				end
			end
		else
			vars = BarMath_Option_CheckBoxes[bar]["Variable"]
		end
		local value = BarMath_GetCharVar(vars)
		if (value == nil) then
			value = 0
		end
		getglobal(Pre..bar..Post):SetChecked(value)

		-- Backup Variables for Reset
		VarsBackup["CheckBoxs"][bar] = {
			["loc"] = vars,
			["val"] = value,
		}
	end

	if (not BarMath_Options_Rounding_Pulldown) then
		f = CreateFrame("Frame", "BarMath_Options_Rounding_Pulldown", BarMath_Options, "UIDropDownMenuTemplate") 
		f:SetPoint("TOPLEFT",BarMath_Options,"TOPLEFT",225,-40)
		UIDropDownMenu_SetWidth(f, 150)
	end

	-- Disable Functions
	BarMath_OptionsResetPBarButton:Disable()
	BarMath_OptionsAllowSnapToCheckBox:Disable()
	BarMath_OptionsAllowSnapToText:SetTextColor(0.5,0.5,0.5)

	-- Initialize Rounding Pulldown
	UIDropDownMenu_Initialize(BarMath_Options_Rounding_Pulldown, BarMath_Options_Basic_RoundingDropDown_Initialize)
	BarMath_Options_Rounding_Pulldown:Show()
	UIDropDownMenu_SetText(BarMath_Options_Rounding_Pulldown, BarMath_GetCharVar("Rounding"),BarMath_Options_Rounding_Pulldown)


	-- Populate Side List
	BarMath_Options_PopulateList()

	-- Show First Tab
	BarMath_Options_AddonList_OnClick(getglobal(BarMath_CurrentButton))
end

function BarMath_Options_Reset()
	-- Checkbox Reset
	for bar,var in pairs(VarsBackup["CheckBoxs"]) do
		BarMath_SetCharVar(VarsBackup["CheckBoxs"][bar]["val"],VarsBackup["CheckBoxs"][bar]["loc"])
	end

	-- Update the Options Window, and the World.
	BarMath_Options_OnShow()
	BarMath_Options_Update()
end

function BarMath_Options_Reset2Defaults()

end

function BarMath_Options_AddonList_OnClick(frame)
	local name = frame:GetName()
	BarMath_CurrentButton = name
	local addon = BarMathOptions_AddonBtnList[name]
	local numtabs = #BarMath_Option_Addons[addon]["Tabs"]
	local GeneralFrame = "BarMath_Options_"..addon.."GeneralFrame"
	if numtabs == nil then
		numtabs = 0
	end

	BarMath_Msg("Numtabs:"..numtabs,"debug")

	local x = 0
	for k,v in pairs(BarMathOptions_AddonBtnList) do
		x = x + 1
		local c = getglobal("BarMath_OptionsAddonListingBtn"..x)
		if (c:GetName() == name) then
			c:SetNormalFontObject(GameFontHighlight)
			getglobal("BarMath_OptionsAddonListingBtn"..x.."BG"):SetAlpha("0.5")
		else
			c:SetNormalFontObject(GameFontNormal)
			getglobal("BarMath_OptionsAddonListingBtn"..x.."BG"):SetAlpha("0")
		end
	end

	BarMath_Msg("Frame Clicked: "..name..", Addon: "..addon,"debug")

	-- Create General Frame
	if ((not getglobal(GeneralFrame)) and (addon ~= "Character")) then
		local f = CreateFrame("Frame",GeneralFrame,BarMath_OptionsAddonPage,"BarMath_Options_GeneralTab")
		f:SetPoint("TOPLEFT",BarMath_OptionsAddonPage,"TOPLEFT", 0, 0)
		f:SetFrameLevel(BarMath_OptionsAddonPage:GetFrameLevel()+1)
	end

	-- Hide all Tabs
	for x=1,BarMath_MaxTabs do
		local btnname = "BarMath_OptionsTab"..x
		getglobal(btnname):Hide()
	end

	-- Hide All Windows
	BarMath_Options_CharacterList:Hide()
	for k,v in pairs (BarMath_Option_Addons) do
		if getglobal("BarMath_Options_"..k.."GeneralFrame") then
			getglobal("BarMath_Options_"..k.."GeneralFrame"):Hide()
		end
		local knumtabs = #BarMath_Option_Addons[k]["Tabs"]
		-- Hide all tab frames
		for x=2, knumtabs+1 do
			local tabpagename = "BarMath_Options_"..k.."Tab"..x.."Page"
			if getglobal(tabpagename) then
				getglobal(tabpagename):Hide()
			end
		end
	end

	-- Show/Hide Character List
	if (addon == "Character") then
		BarMath_Options_CharacterList:Show()
		-- We're all done with the Character List!
		return
	else
		BarMath_OptionsTab1:Show()
		BarMath_Options_CharacterList:Hide()
	end

	-- Show the General Tab Frame for any surviving pages!
	if (getglobal(GeneralFrame)) then
		getglobal(GeneralFrame):Show()
		BarMath_Options_GeneralFrameUpdate(getglobal(GeneralFrame),addon)
		BarMath_Msg("Showing the General Tab for "..addon,"debug")
	end

	-- Kill the rest of this function if the addon has too many tabs.
	if (numtabs > BarMath_MaxTabs) then
		BarMath_Msg(BARMATH_DEVERROR_TOOMANYTABS..BarMath_MaxTabs,"Error")
		return
	end

	-- Generate Tab Buttons
	if (numtabs > 0) then
		for x=1,numtabs do
			local btnname = "BarMath_OptionsTab"..(x+1)
			local btntext = BarMath_Option_Addons[addon]["Tabs"][x]["TabTitle"]

			-- set Text
			BarMath_Msg("Setting Text for Tab "..x.." to \""..btntext.."\"","debug")
			getglobal(btnname.."Text"):SetText(btntext)
			PanelTemplates_TabResize(getglobal(btnname), 0)
			getglobal(btnname.."HighlightTexture"):SetWidth(getglobal(btnname):GetTextWidth() + 30)
			getglobal(btnname):Show()
			PanelTemplates_DeselectTab(getglobal(btnname))
		end
		PanelTemplates_SetNumTabs(BarMath_Options, numtabs)
		PanelTemplates_SetTab(BarMath_Options, 1)
		BarMath_CurrentButtonTab = 1
	end
end

function BarMath_Options_GeneralFrameCheckBox(self)
	local name = self:GetName()
	local value = self:GetChecked()
	local GframeStart,GframeEnd = strfind(name, "GeneralFrame")
	local addon = strsub(name, 17, GframeStart-1)

	if value == nil then
		value = 0
	end

	for i=1, bm.DisplayCount do
		local checkbox = "BarMath_Options_"..addon.."GeneralFramePulldown"..i.."CheckBox"
		if (checkbox == name) then
			BarMath_SetCharVar(value,"Bars",addon.."Bar","Displays","Display"..i.."OnOff")
			if value == 0 then
				UIDropDownMenu_DisableDropDown(getglobal("BarMath_Options_"..addon.."GeneralFramePulldown"..i.."DropDownList1"))
			else
				UIDropDownMenu_EnableDropDown(getglobal("BarMath_Options_"..addon.."GeneralFramePulldown"..i.."DropDownList1"))
			end
		end
	end
	BarMath_Options_Update()
end

function BarMath_Options_GeneralFrameUpdate(frame, addon)
	local name = frame:GetName()
	if (not addon) then
		local GframeStart,GframeEnd = strfind(name, "GeneralFrame")
		addon = strsub(name, 17, GframeStart-1)
	end

	for i=1, bm.DisplayCount do
		local Pulldown = getglobal("BarMath_Options_"..addon.."GeneralFramePulldown"..i.."DropDownList1")
		local checkbox = getglobal("BarMath_Options_"..addon.."GeneralFramePulldown"..i.."CheckBox")
		local chkbxval = BarMath_GetCharVar("Bars",addon.."Bar","Displays","Display"..i.."OnOff")
		checkbox:SetChecked(chkbxval)
		if chkbxval == 0 then
			UIDropDownMenu_DisableDropDown(getglobal("BarMath_Options_"..addon.."GeneralFramePulldown"..i.."DropDownList1"))
		else
			UIDropDownMenu_EnableDropDown(getglobal("BarMath_Options_"..addon.."GeneralFramePulldown"..i.."DropDownList1"))
		end

		local options = BarMath_Option_Addons[addon]["Pulldown"]
		local dispval = BarMath_GetCharVar("Bars",addon.."Bar","Displays","Display"..i)

		UIDropDownMenu_SetSelectedValue(Pulldown,dispval,options[dispval])
		UIDropDownMenu_SetText(Pulldown,getglobal(options[dispval]))
	end
end

function BarMath_Options_Basic_RoundingDropDown_Initialize()
	local level, checked = 1, nil
	local info = UIDropDownMenu_CreateInfo()

	if (BarMath_GetCharVar("Rounding") == BARMATH_ROUNDING_DOWN) then
		checked = 1
	else
		checked = nil
	end

	info.text = BARMATH_ROUNDING_DOWN
	info.value = BARMATH_ROUNDING_DOWN
	info.func = BarMath_Options_Basic_RoundingDropDown_OnClick
	info.owner = BarMath_Options_Rounding_Pulldown
	info.checked = checked
	info.icon = nil
	UIDropDownMenu_AddButton(info, level)

	if (BarMath_GetCharVar("Rounding") == BARMATH_ROUNDING_UP) then
		checked = 1
	else
		checked = nil
	end

	info.text = BARMATH_ROUNDING_UP
	info.value = BARMATH_ROUNDING_UP
	info.func = BarMath_Options_Basic_RoundingDropDown_OnClick
	info.owner = BarMath_Options_Rounding_Pulldown
	info.checked = checked
	info.icon = nil
	UIDropDownMenu_AddButton(info, level)

	if (BarMath_GetCharVar("Rounding") == BARMATH_ROUNDING_STANDARD) then
		checked = 1
	else
		checked = nil
	end

	info.text = BARMATH_ROUNDING_STANDARD 
	info.value = BARMATH_ROUNDING_STANDARD 
	info.func = BarMath_Options_Basic_RoundingDropDown_OnClick 
	info.owner = BarMath_Options_Rounding_Pulldown 
	info.checked = checked
	info.icon = nil 
	UIDropDownMenu_AddButton(info, level) 
end

function BarMath_Options_Basic_RoundingDropDown_OnClick(self)
	UIDropDownMenu_SetSelectedValue(self.owner, self.value)

	BarMath_SetCharVar(self.value,"Rounding")
end 

function BarMath_Options_Pulldown1_Init(frame)
	local name = frame:GetName()
	local bartitle = strsub(name, 17, strfind(name,"GeneralFrame")-1)
	local options = BarMath_Option_Addons[bartitle]["Pulldown"]

	for i=1,#options do
		local checked
		for x=1,5 do
			if (options[i] == BarMath_GetCharVar("Bars",bartitle.."Bar","Displays","Display"..x)) then
				checked = 1
			else
				checked = nil
			end
		end
		local info = UIDropDownMenu_CreateInfo()

		info.text = getglobal(options[i])
		info.value = options[i]
		info.func = function() BarMath_Options_GeneralTab_PullDown_OnClick(frame, bartitle, i, options[i]) end
		info.owner = frame
		info.checked = checked
		UIDropDownMenu_AddButton(info, 1)
	end
end

function BarMath_Options_Pulldown2_Init(frame)
	local name = frame:GetName()
	local bartitle = strsub(name, 17, strfind(name,"GeneralFrame")-1)
	local options = BarMath_Option_PullDown_RawBarsPercent

	for i=1,#options do
		local info = UIDropDownMenu_CreateInfo()

		info.text = getglobal(options[i])
		info.value = options[i]
		info.func = function() BarMath_Options_GeneralTab_PullDown_OnClick(frame, bartitle, i, options[i]) end
		info.owner = frame
		info.checked = nil
		UIDropDownMenu_AddButton(info, 1)
	end
end

function BarMath_Options_BuildPulldownMenu(BarTitle,OptionList)
	for i=1,#OptionList do
		local info = UIDropDownMenu_CreateInfo()

		info.text = OptionList[i]
		info.value = i
		info.func = function() BarMath_Options_GeneralTab_PullDown_OnClick(frame, BarTitle, OptionList[i]) end
		info.owner = self
		info.checked = nil
		UIDropDownMenu_AddButton(info, 1)
	end
end

function BarMath_Options_GeneralTab_PullDown_OnClick(self, BarTitle, num, option)
	UIDropDownMenu_SetSelectedValue(self, self.value, getglobal(option))
	local displaynum = strsub(self:GetParent():GetName(),-1)

	-- BarMath_Msg("Setting "..BarTitle.."'s Value for Display"..displaynum.." to "..num..", which should be the numerical value of "..tostring(getglobal(option)))

	BarMath_SetCharVar(num,"Bars", BarTitle.."Bar","Displays","Display"..displaynum)
	UIDropDownMenu_SetText(self,getglobal(option))
	BarMath_Options_Update()
end 

function BarMath_Options_GeneralTab_OnShow(self)
	local tagt = self:GetName()
	local bartitle = strsub(tagt, 17, strfind(tagt,"GeneralFrame")-1)

	-- Set Static Text
--	getglobal(tagt.."GeneralOptionsTitleText"):SetText(BARMATH_OPTIONS_GENERAL_TAB)
--	getglobal(tagt.."BarTextOptionsText"):SetText(BARMATH_OPTIONS_GENERAL_BARTEXT)
	getglobal(tagt.."AlwaysShowTextText"):SetText(BARMATH_OPTIONS_GENERAL_SHOWBARTXT_CHECKBOX)

	local AlwaysShow = BarMath_GetCharVar("Bars",bartitle.."Bar","DisplayMethod")
	getglobal(tagt.."AlwaysShowTextCheckBox"):SetChecked(AlwaysShow)

	for i=1,bm.DisplayCount do
		local chkbx = getglobal(tagt.."Pulldown"..i.."CheckBox")
		chkbx:SetChecked(BarMath_GetCharVar("Bars",bartitle.."Bar","Displays","Display"..i.."OnOff"))

		local pldn1 = getglobal(tagt.."Pulldown"..i.."DropDownList1")
		local pldn2 = getglobal(tagt.."Pulldown"..i.."DropDownList2")
		UIDropDownMenu_SetWidth(pldn1, 230)
		UIDropDownMenu_SetWidth(pldn2, 150)
		-- Initialize Pulldowns
		UIDropDownMenu_Initialize(pldn1, BarMath_Options_Pulldown1_Init)
		UIDropDownMenu_Initialize(pldn2, BarMath_Options_Pulldown2_Init)
		pldn1:Show()
		pldn2:Hide()
	end
	BarMath_Options_GeneralFrameUpdate(self)
end

function BarMath_Options_GeneralCheckboxOnOff(frame, value)
	local tagt = frame:GetName()
	local GframeStart,GframeEnd = strfind(tagt, "GeneralFrame")
	local bartitle = strsub(tagt, 17, GframeStart-1)
	local checkbox = strsub(tagt, GframeEnd+1)

	if (checkbox == "AlwaysShowText") then
		BarMath_SetCharVar(value,"Bars",bartitle.."Bar","DisplayMethod")
	end
	BarMath_Options_Update()
end

function BarMath_Options_Tab_OnClick(self, button)
	local name = self:GetName()
	local num = tonumber(strsub(name,strfind(name,"%d")))
	local addon = BarMathOptions_AddonBtnList[BarMath_CurrentButton]
	local numtabs = #BarMath_Option_Addons[addon]["Tabs"]
	local GeneralFrame = "BarMath_Options_"..addon.."GeneralFrame"
	local thispage = "BarMath_Options_"..addon.."Tab"..num.."Page"

	BarMath_Msg("tab Num: "..tostring(num),"debug")

	-- Hide all tab frames
	for x=2, numtabs+1 do
		local tabpagename = "BarMath_Options_"..addon.."Tab"..x.."Page"
		if getglobal(tabpagename) then
			getglobal(tabpagename):Hide()
		end
	end

	PlaySound("igCharacterInfoTab")

	-- Show/Hide General Tab
	if (num == 1) then
		getglobal(GeneralFrame):Show()
		return
	else
		getglobal(GeneralFrame):Hide()
	end

	local thistemplate = BarMath_Option_Addons[addon]["Tabs"][(num-1)]["TabTemp"]
	BarMath_Msg("Template: "..thistemplate,"debug")

	if not getglobal(thispage) then
		local f = CreateFrame("Frame",thispage,BarMath_OptionsAddonPage,thistemplate)
		f:SetPoint("TOPLEFT",BarMath_OptionsAddonPage,"TOPLEFT", 0, 0)
		f:SetFrameLevel(BarMath_OptionsAddonPage:GetFrameLevel()+1)
	end

	getglobal(thispage):Show()
	BarMath_CurrentButtonTab = num
	PanelTemplates_SetTab(BarMath_Options,BarMath_CurrentButtonTab)

	if getglobal(thispage):IsVisible() then
		BarMath_Msg("TabPage should be shown.","debug")
	end
end

function BarMath_CharacterList_GenerateList()
	local data = {}
	local current = 0

	for realm,v in pairs(BarMath_Characters) do
		for faction,f in pairs(BarMath_Characters[realm]) do
			for char,c in pairs(BarMath_Characters[realm][faction]) do
				local name = char.." of "..realm
				if (BarMath_GetCharVar("CharListShowOtherFactions")==1)or((BarMath_GetCharVar("CharListShowOtherFactions") == 0)and(faction == BarMath_Faction)) then	
					if (BarMath_GetCharVar("CharListShowOtherServers")==1)or((BarMath_GetCharVar("CharListShowOtherServers") == 0)and(realm == BarMath_Realm)) then	
						current = current + 1
						data[current] = {
							["Data"] = BarMath_Characters[realm][faction][char],
							["Realm"] = tostring(realm),
							["Faction"] = tostring(faction),
							["CharName"] = tostring(char),
						}
					end
				end
			end
		end
	end

	return data
end

function BarMath_GetToonData(data,toon,var1,var2,var3,var4,var5)
	local output = BARMATH_TXT_UNKNOWN

	if (not data) then
		return output
	end
	if (not data[toon]) then
		return output
	end

	if (data[toon][var1] ~= nil) then
		if (type(data[toon][var1]) == "table") then
			if (data[toon][var1][var2] ~= nil) then
				if (type(data[toon][var1][var2]) == "table") then
					if (data[toon][var1][var2][var3] ~= nil) then
						if (type(data[toon][var1][var2][var3]) == "table") then
							if (data[toon][var1][var2][var3][var4] ~= nil) then
								if (type(data[toon][var1][var2][var3][var4]) == "table") then
									if (data[toon][var1][var2][var3][var4][var5] ~= nil) then
										return data[toon][var1][var2][var3][var4][var5]
									end
								else
									return data[toon][var1][var2][var3][var4]
								end
							end
						else
							return data[toon][var1][var2][var3]
						end
					end
				else
					return data[toon][var1][var2]
				end
			end
		else
			return data[toon][var1]
		end
	end
	return output
end

function BarMath_CharacterList_PopulateList(self)
	local data = BarMath_CharacterList_GenerateList()
	local current, line
	local total = 12
	local totalchars = #data

	for line=1,12 do
		current = line + FauxScrollFrame_GetOffset(BarMath_CharacterListScrollBar)
		if (current <= totalchars) then
			local charline = "BarMath_Options_CharacterListCharName"..line
			local faction, realm = data[current]["Faction"], data[current]["Realm"]
			local sex, sextxt = BarMath_GetToonData(data,current,"Data","ToonData","Sex"), BARMATH_TXT_UNKNOWN
			local lvl, lvltxt = BarMath_GetToonData(data,current,"Data","ToonData","Level"), "??"

			if (lvl ~= BARMATH_TXT_UNKNOWN) then
				lvltxt = lvl
			end

			if (sex == 2) then
				sextxt = BARMATH_TXT_MALE
			elseif (sex == 3) then
				sextxt = BARMATH_TXT_FEMALE
			end

			_G[charline.."CharName"]:SetText(data[current]["CharName"])
			getglobal("BarMath_Options_CharacterListCharName"..line.."Server"):SetText(realm)

			if (faction==BARMATH_TXT_HORDE) then
				icon = "Interface/GroupFrame/UI-Group-PVP-Horde"
			else
				icon = "Interface/GroupFrame/UI-Group-PVP-Alliance"
			end

			_G[charline.."FactionIcon"]:SetTexture(icon)
			getglobal("BarMath_Options_CharacterListCharName"..line.."Faction"):SetText(faction)
			getglobal("BarMath_Options_CharacterListCharName"..line.."Level"):SetText(lvltxt)
			getglobal("BarMath_Options_CharacterListCharName"..line.."Class"):SetText(BarMath_GetToonData(data,current,"Data","ToonData","Class"))
			getglobal("BarMath_Options_CharacterListCharName"..line.."Race"):SetText(BarMath_GetToonData(data,current,"Data","ToonData","Race"))
			getglobal("BarMath_Options_CharacterListCharName"..line.."Sex"):SetText(sextxt)
			getglobal("BarMath_Options_CharacterListCharName"..line):Show()
		else
			getglobal("BarMath_Options_CharacterListCharName"..line):Hide()
		end
	end

	BarMath_Options_CharacterList:Show()
end

function BarMath_CharacterList_ScrollUpdate(self)
	FauxScrollFrame_Update(BarMath_CharacterListScrollBar,BarMath_TotalChars,12,1)
	BarMath_CharacterList_PopulateList()
end

function BarMath_CharacterList_OnClick(self)
	local data = BarMath_CharacterList_GenerateList()
	local current, line

	if (not self) then
		for line=1,12 do
			current = line + FauxScrollFrame_GetOffset(BarMath_CharacterListScrollBar)
			getglobal("BarMath_Options_CharacterListCharName"..line.."BG"):SetTexture("0.5","0.5","1","0.2")
		end
		return
	end

	for line=1,12 do
		current = line + FauxScrollFrame_GetOffset(BarMath_CharacterListScrollBar)
		getglobal("BarMath_Options_CharacterListCharName"..line.."BG"):SetTexture("0.5","0.5","1","0.2")
		if (self:GetName() ==  "BarMath_Options_CharacterListCharName"..current) then
			BarMath_CurrentToon = current
		end
	end
	
	getglobal(self:GetName().."BG"):SetTexture("0.5","0","0","0.5")
end

function BarMath_CharacterList_OnShow(self)
	if (self:IsVisible()) then
		BarMath_Msg(self:GetName().." should be visible.","debug")
	end
end

function BarMath_CharacterList_OnHide(self)
	if (not self:IsVisible()) then
		BarMath_Msg(self:GetName().." should not be visible.","debug")
	end
end

function BarMath_CharList_DeleteSettings()
	local data = BarMath_CharacterList_GenerateList()
	local newsettings = {}
	local remove, success

	if (not data[BarMath_CurrentToon]) then
		return
	end

	if (data[BarMath_CurrentToon]["CharName"] == BarMath_PlayerName) and (data[BarMath_CurrentToon]["Realm"] == BarMath_Realm) then
		BarMath_Msg("Can not delete your own settings!","Error")
		return
	end

	local settings = BarMath_Characters

	for realm,v in pairs(settings) do
		for faction,f in pairs(settings[realm]) do
			for char,c in pairs(settings[realm][faction]) do
				if (char ~= data[BarMath_CurrentToon]["CharName"]) then
					if (not newsettings[realm]) then
						newsettings[realm] = {}
					end
					if (not newsettings[realm][faction]) then
						newsettings[realm][faction] = {}
					end
					if (not newsettings[realm][faction][char]) then
						newsettings[realm][faction][char] = c
					end
				end
			end
		end
	end

	for addon,v in pairs(BarMath_BarInfoDelete) do
		remove = getglobal(BarMath_BarInfoDelete[addon])(data[BarMath_CurrentToon]["Realm"], data[BarMath_CurrentToon]["Faction"], data[BarMath_CurrentToon]["CharName"])
		if (remove == "true") then
			BarMath_Msg("Data Successfully Removed","debug")
			success = "true"
		end
	end

	if (success ~= nil and success == "true") then
		BarMath_Characters = newsettings
		BarMath_Msg(data[BarMath_CurrentToon]["CharName"].."'s settings have been deleted.")
		BarMath_CharacterList_PopulateList()
		BarMath_CharacterList_OnClick()
		BarMath_CurrentToon = ""
	else
		BarMath_Msg("Could not remove data from the Info database. Character not deleted.","Error")
	end
end

function BarMath_Tooltip_Generate(frame,title,text,anchor)
	if anchor == nil then
		anchor = "ANCHOR_CURSOR"
	end
	if title == nil then
		title = "nil"
	end
	if text == nil then
		text = "nil"
	end

	BarMathTooltip:SetOwner(frame, anchor)
	BarMathTooltip:SetText(title)
	BarMathTooltip:AddLine(text,1,1,1)
	BarMathTooltip:Show()
end