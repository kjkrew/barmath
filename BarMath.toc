## Title: BarMath |CFFFF0000Core|r
## Interface: 50100
## Version: <%version%>
## Author: Kjasi
## Notes: Does math for you, and displays it on your XP, Rep, and Custom Bars!
## URL: http://code.google.com/p/kjasiwowaddons/
## DefaultState: Enabled
## LoadOnDemand: 0
## X-Category: Information
## SavedVariables: BarMath_Characters, BarMath_AddonData
Load.xml