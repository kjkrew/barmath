--[[
	BarMath v2.0.5
]]

BarMath_Version = "2.0.6";
local WoW_Version = "30200";
BarMath_NumBars = 20;
BarMath_DisplayCount = 5;
local BarMath_Debug = 10;
local BarMath_Color_01 = "|CFF65DF65"; -- Standard BarMath Color
local BarMath_Color_02 = "|CFF5555FF"; -- Debug Color

BarMath_Realm = GetRealmName();
BarMath_Faction = UnitFactionGroup("player");
BarMath_PlayerName = UnitName("player");
BarMath_Class, BarMath_EngClass = UnitClass("player");
BarMath_NotFaction = nil;

BarMath_Barcount = 0;
BarMath_BarList = {};
BarMath_EventList = {};
BarMath_BarInfo = {};
BarMath_BarInfoDelete = {};
BarMath_Bar_Height = 12;

BarMath_Debug = 0; -- 0 turns off debug msgs, 1 turns them off.

BarMath_InVehicle = false;
local BMPos_Bkup = UIPARENT_MANAGED_FRAME_POSITIONS;

-- Global to determine the maximum level obtainable.
BarMath_MaxLVL = 80;

BarMath_HaveLoaded = {
	["Addons"] = {},
	["Bars"] = {},
}

BarMath_CurrentBarOrder = -1;

if (BarMath_Faction == BARMATH_TXT_ALLIANCE) then
	BarMath_NotFaction = BARMATH_TXT_HORDE;
else
	BarMath_NotFaction = BARMATH_TXT_ALLIANCE;
end

local Loaded = false;
local BarMath_NewPlayer = false;

local BarMath_Defaults = {
	["Options"] = {
		["Version"] = BarMath_Version,
		["Rounding"] = BARMATH_ROUNDING_DOWN, -- Other Values: BARMATH_ROUNDING_UP, BARMATH_ROUNDING_STANDARD
		["Decimals"] = 2,
		["ShowTooltips"] = 1, -- 1 = On, 0 = Off
		["ShowParentBar"] = 0,
		["ShowMinimapButton"] = 1,
		["ShowOverBar"] = 1,
		["TrimNumbers"] = 0,
		["CharListShowOtherFactions"] = 1,
		["CharListShowOtherServers"] = 1,
		["AllowSnapToZones"] = 1,
		["UseCommas"] = 0,
		["ToonData"] = {
			["Race"] = UnitRace("player"),
			["Class"] = UnitClass("player"),
			["Level"] = UnitLevel("player"),
			["Sex"] = UnitSex("player"),
		},
		["Bars"] = {},
	},
	["Bars"] = {					-- Default settings for Bars. Used if custom options not specified.
		["Display"] = 1,			-- 1 = Show Bar, 0 = Hide Bar
		["DisplayMethod"] = 1,			-- 1 = Text is Always on, 0 = Text only when mouseover
		["TextLength"] = BARMATH_TEXT_SHORT,	-- Length of text on the Bars. Other Value: BARMATH_TEXT_LONG
		["Displays"] = {
			["Display1"] = 1,		-- Determines the default option for the Displays.
			["Display2"] = 2,
			["Display3"] = 3,
			["Display4"] = 4,
			["Display5"] = 5,
			["Display1OnOff"] = 1,		-- Default setting for if this Display is visible. 1 = Visible, 0 = Hidden.
			["Display2OnOff"] = 1,
			["Display3OnOff"] = 1,
			["Display4OnOff"] = 1,
			["Display5OnOff"] = 1,					
		},
	},
}

-- Message output function.
function BarMath_Msg(msg, channel)
	if (msg == nil) then
		return;
	end
	-- User Error Message Channel
	if (channel == "Error") then
		DEFAULT_CHAT_FRAME:AddMessage(RED_FONT_COLOR_CODE..BARMATH_TITLE.." "..BARMATH_TXT_ERROR..": |r"..msg);

	-- Developer Error Message Channel
	elseif (channel == "DevError") then
		DEFAULT_CHAT_FRAME:AddMessage(RED_FONT_COLOR_CODE..BARMATH_TITLE.." "..BARMATH_TXT_DEVERROR..": |r"..msg);

	-- Debug Message Channel
	elseif (channel == "debug") or (channel == "Debug") then
		if (BarMath_Debug == 1) then
			DEFAULT_CHAT_FRAME:AddMessage(BarMath_Color_02..BARMATH_TITLE.." "..BARMATH_TXT_DEBUG..": |r"..tostring(msg));
		end

	-- All Other Messages
	else
		DEFAULT_CHAT_FRAME:AddMessage(BarMath_Color_01..BARMATH_TITLE..": |r"..tostring(msg));
	end
end

-- Should be used for Big User Errors, such as the Database being destroyed and rebuilt.
function BarMath_BigError_Msg(msg)
	DEFAULT_CHAT_FRAME:AddMessage("|CFFFF0000"..BARMATH_TITLE.." "..BARMATH_TXT_ERROR..": |r"..msg);
	UIErrorsFrame:AddMessage("|CFFFF0000"..BARMATH_TITLE.." "..BARMATH_TXT_ERROR.." |r"..msg, 1.0, 1.0, 1.0);
end

function BarMath_Load(self)
	self:RegisterEvent("VARIABLES_LOADED");
	self:RegisterEvent("PLAYER_LEVEL_UP");
	self:RegisterEvent("PLAYER_ENTERING_WORLD");
	self:RegisterEvent("UNIT_ENTERING_VEHICLE");
	self:RegisterEvent("UNIT_ENTERED_VEHICLE");
	self:RegisterEvent("UNIT_EXITING_VEHICLE");
	self:RegisterEvent("UNIT_EXITED_VEHICLE");
	self:RegisterEvent("PLAYER_REGEN_ENABLED");
	self:RegisterEvent("PLAYER_LEAVE_COMBAT");

	-- Slash Commands
	SLASH_BarMath1 = "/barmath";
	SLASH_BarMath2 = "/bm";
	SlashCmdList["BarMath"] = BarMath_commandline;

	orig_UIParent_ManageFramePositions = UIParent_ManageFramePositions;
	UIParent_ManageFramePositions = BarMath_ManageFramePositions;
--	hooksecurefunc("VehicleSeatIndicator_UnloadTextures", BarMath_Update_All);
end

function BarMath_ManageFramePositions()
	if BarMath_InVehicle == false then
		if (not InCombatLockdown()) then
			orig_UIParent_ManageFramePositions();
		end
	else
		orig_UIParent_ManageFramePositions();
	end	
end

function BarMath_OnEvent(self, event, ...)
	local arg1, arg2, arg3 = ...;
	if event == "VARIABLES_LOADED" then
		BarMath_Loaded(self);
	elseif event == "PLAYER_ENTERING_WORLD" then
		BarMath_Generate_Database();
		BarMath_InVehicle = false;
		BarMath_BarFix();
	elseif event == "PLAYER_LEVEL_UP" then
		BarMath_SetVar(arg1, "ToonData","Level");
	elseif (event == "PLAYER_REGEN_ENABLED") or (event == "PLAYER_LEAVE_COMBAT") then
		BarMath_BarFix();
	elseif (event == "UNIT_ENTERING_VEHICLE" and arg1 == "player") then
	elseif (event == "UNIT_EXITING_VEHICLE" and arg1 == "player") then
		securecall(MainMenuBar_UpdateArt,MainMenuBar);
	elseif (event == "UNIT_ENTERED_VEHICLE" and arg1 == "player") then
	elseif (event == "UNIT_EXITED_VEHICLE" and arg1 == "player") then
		securecall(MainMenuBar_UpdateArt,MainMenuBar);
	else
		for k,v in pairs(BarMath_EventList) do
			if (event == k) then
				for y=1,#v do
					v[y](...);
				end
			end
		end
	end
end

function BarMath_Generate_Database()
	if (not BarMath_CharSettings) then
		BarMath_CharSettings = {};
		BarMath_NewPlayer = true;

	end
	if (not BarMath_CharSettings[BarMath_Realm]) then
		BarMath_CharSettings[BarMath_Realm] = {};
		BarMath_NewPlayer = true;

	end
	if (not BarMath_CharSettings[BarMath_Realm][BarMath_Faction]) then
		BarMath_CharSettings[BarMath_Realm][BarMath_Faction] = {};
		BarMath_NewPlayer = true;

	end
	if (not BarMath_CharSettings[BarMath_Realm][BarMath_Faction][BarMath_PlayerName]) then
		BarMath_CharSettings[BarMath_Realm][BarMath_Faction][BarMath_PlayerName] = BarMath_Defaults["Options"];
		BarMath_NewPlayer = true;

	end
	local CharDB = BarMath_CharSettings[BarMath_Realm][BarMath_Faction][BarMath_PlayerName];

	if (not CharDB["Version"]) or (CharDB["Version"] < BarMath_Version) then
		BarMath_OptionUpdate();
	end

	if (not BarMath_Info) then
		BarMath_Info = {};
		BarMath_NewPlayer = true;
	end
end

function BarMath_Info_Generate(addon,database)
	if ((not addon) or (type(addon) == "table")) then
		BarMath_Msg(BARMATH_DEVERROR_INFODB_GENERATIONFAILED, "DevError")
		return;
	end

	BarMath_Generate_Database();
	if (not BarMath_Info[addon]) then
		BarMath_Info[addon] = {};
	end

	for k,v in pairs(database) do
		if (type(v) == "table") then
			if (not BarMath_Info[addon][k]) then
				BarMath_Info[addon][k] = {};
			end
			for k2,v2 in pairs(database[k]) do
				if (type(v2) == "table") then
					if (not BarMath_Info[addon][k][k2]) then
						BarMath_Info[addon][k][k2] = {};
					end
					for k3,v3 in pairs(database[k][k2]) do
						if (type(v3) == "table") then
							if (not BarMath_Info[addon][k][k2][k3]) then
								BarMath_Info[addon][k][k2][k3] = {};
							end
							for k4,v4 in pairs(database[k][k2][k3]) do
								if (type(v4) == "table") then
									if (not BarMath_Info[addon][k][k2][k3][k4]) then
										BarMath_Info[addon][k][k2][k3][k4] = {};
									end
									for k5,v5 in pairs(database[k][k2][k3][k4]) do
										if (type(v5) == "table") then
											if (not BarMath_Info[addon][k][k2][k3][k4][k5]) then
												BarMath_Info[addon][k][k2][k3][k4][k5] = {};
											end
											for k6,v6 in pairs(database[k][k2][k3][k4][k5]) do
												if (type(v6) == "table") then
													if (not BarMath_Info[addon][k][k2][k3][k4][k6]) then
														BarMath_Info[addon][k][k2][k3][k4][k6] = {};
													end
													for k7,v7 in pairs(database[k][k2][k3][k4][k6]) do
														if (type(v7) == "table") then
															BarMath_Msg(BARMATH_DEVERROR_INFODB_TOOMANYLEVELS,"DevError");
														elseif ((not BarMath_Info[addon][k][k2][k3][k4][k5][k6][k7])and(k7~=nil and v7~=nil)) then
															BarMath_Info[addon][k][k2][k3][k4][k5][k6][k7] = v7;
														end
													end
												elseif ((not BarMath_Info[addon][k][k2][k3][k4][k5][k6])and(k6~=nil and v6~=nil)) then
													BarMath_Info[addon][k][k2][k3][k4][k5][k6] = v6;
												end
											end
										elseif ((not BarMath_Info[addon][k][k2][k3][k4][k5])and(k5~=nil and v5~=nil)) then
											BarMath_Info[addon][k][k2][k3][k4][k5] = v5;
										end
									end
								elseif ((not BarMath_Info[addon][k][k2][k3][k4])and(k4~=nil and v4~=nil)) then
									BarMath_Info[addon][k][k2][k3][k4] = v4;
								end
							end
						elseif ((not BarMath_Info[addon][k][k2][k3])and(k3~=nil and v3~=nil)) then
							BarMath_Info[addon][k][k2][k3] = v3;
						end
					end
				elseif ((not BarMath_Info[addon][k][k2])and(k2~=nil and v2~=nil)) then
					BarMath_Info[addon][k][k2] = v2;
				end
			end
		elseif ((not BarMath_Info[addon][k])and(k~=nil and v~=nil)) then
			BarMath_Info[addon][k] = v;
		end
	end
end

function BarMath_Update_All()
	--BarMath_Msg("Updating All...")
	for x=1,#BarMath_BarList do
		local update = getglobal(BarMath_BarList[x]["Update"]);
		update();
		BarMath_Bar_ShowHide(BarMath_BarList[x]["Name"]);
	--	BarMath_Msg("\""..BarMath_BarList[x]["Update"].."\" Ran, "..BarMath_BarList[x]["Name"].." Bar updated!");
	end
	BarMath_Minimap_OnOff();
	BarMath_ReorderBars(-1);
	BarMath_Post_SetParentHeight();
	BarMath_BarAdjust();
end

function BarMath_Loaded(self)
	BarMath_Generate_Database();
	BarMath_Minimap_OnOff();
	Loaded = true;

	BarMath_RepTime = {};

	for k,v in pairs(BarMath_EventList) do
		self:RegisterEvent(k);
		--BarMath_Msg("Event Registered: \""..k.."\"");
	end

	if (BarMath_NewPlayer == true) then
		BarMath_Msg("New Player Found.","debug")
		BarMath_ParentBar_Reset();
		BarMath_Update_All();
		BarMath_NewPlayer = false;
	end
	
	BarMath_Post_Load();

	BarMath_Msg(BARMATH_TXT_VERSION.." "..BarMath_Version.." "..BARMATH_TXT_ISNOWLOADED);
	for k,v in pairs(BarMath_HaveLoaded["Addons"]) do
		BarMath_Msg(v);
	end
end

function BarMath_Minimap_OnOff()
	if BarMath_GetVar("ShowMinimapButton") == 0 then
		BarMathMinimapButton:Hide();
	else
		BarMathMinimapButton:Show();
	end
end

function BarMath_OptionUpdate()
	local temp = BarMath_Defaults["Options"];
	if (BarMath_GetVar("Version") < "2.0") then
		BarMath_CharSettings = {};
		BarMath_Info = {};
		BarMath_Generate_Database();
		BarMath_BigError_Msg(BARMATH_ERROR_DATABASE_OUTOFDATEANDDELETED);
		BarMath_ParentBar_Reset();
		BarMath_Update_All();
	else
		local CharDB = BarMath_CharSettings[BarMath_Realm][BarMath_Faction][BarMath_PlayerName];
		for k,v in pairs(CharDB) do
			if (type(temp[k])=="table") and (type(CharDB[k]) == "table") then
				for k2,v2 in pairs(CharDB[k]) do
					if (type(temp[k][k2])=="table") and (type(CharDB[k][k2]) == "table") then
						for k3,v3 in pairs(CharDB[k][k2]) do
							if (type(temp[k][k2][k3])=="table") and (type(CharDB[k][k2][k3]) == "table") then
								for k4,v4 in pairs(CharDB[k][k2][k3]) do
									if (type(temp[k][k2][k3][k4])=="table") and (type(CharDB[k][k2][k3][k4]) == "table") then
										for k5,v5 in pairs(CharDB[k][k2][k3][k5]) do
											if ((CharDB[k][k2][k3][k4][k5]) and (v5 ~= nil)) then
												temp[k][k2][k3][k4][k5] = v5;
											end
										end
									elseif ((CharDB[k][k2][k3][k4]) and (v4 ~= nil)) then
										temp[k][k2][k3][k4] = v4;
									end
								end
							elseif ((CharDB[k][k2][k3]) and (v3 ~= nil)) then
								temp[k][k2][k3] = v3;
							end
						end
					elseif ((CharDB[k][k2]) and (v2 ~= nil)) then
						temp[k][k2] = v2;
					end
				end
			elseif ((CharDB[k]) and (v ~= nil)) then
				temp[k] = v;
			end
		end
		temp["Version"] = BarMath_Version;
		BarMath_CharSettings[BarMath_Realm][BarMath_Faction][BarMath_PlayerName] = {}; -- Just in case, erase all previous settings.
		BarMath_CharSettings[BarMath_Realm][BarMath_Faction][BarMath_PlayerName] = temp;
	end
end

function BarMath_AddonOptionUpdate(BarTitle, Defaults)
	local temp = Defaults;
	local CharDB = BarMath_CharSettings[BarMath_Realm][BarMath_Faction][BarMath_PlayerName]["Bars"][BarTitle.."Bar"];
	for k,v in pairs(CharDB) do
		if (type(temp[k])=="table") and (type(CharDB[k]) == "table") then
			for k2,v2 in pairs(CharDB[k]) do
				if (type(temp[k][k2])=="table") and (type(CharDB[k][k2]) == "table") then
					for k3,v3 in pairs(CharDB[k][k2]) do
						if (type(temp[k][k2][k3])=="table") and (type(CharDB[k][k2][k3]) == "table") then
							for k4,v4 in pairs(CharDB[k][k2][k3]) do
								if (type(temp[k][k2][k3][k4])=="table") and (type(CharDB[k][k2][k3][k4]) == "table") then
										for k5,v5 in pairs(CharDB[k][k2][k3][k5]) do
										if ((CharDB[k][k2][k3][k4][k5]) and (v5 ~= nil)) then
											temp[k][k2][k3][k4][k5] = v5;
										end
									end
								elseif ((CharDB[k][k2][k3][k4]) and (v4 ~= nil)) then
									temp[k][k2][k3][k4] = v4;
								end
							end
						elseif ((CharDB[k][k2][k3]) and (v3 ~= nil)) then
							temp[k][k2][k3] = v3;
						end
					end
				elseif ((CharDB[k][k2]) and (v2 ~= nil)) then
					temp[k][k2] = v2;
				end
			end
		elseif ((CharDB[k]) and (v ~= nil)) then
			temp[k] = v;
		end
	end
	temp["Version"] = getglobal("BarMath_"..BarTitle.."_Version");
	BarMath_CharSettings[BarMath_Realm][BarMath_Faction][BarMath_PlayerName]["Bars"][BarTitle.."Bar"] = {}; -- Just in case, erase all previous settings.
	BarMath_CharSettings[BarMath_Realm][BarMath_Faction][BarMath_PlayerName]["Bars"][BarTitle.."Bar"] = temp;
end

function BarMath_GetVar(lvl1, lvl2, lvl3, lvl4, lvl5)
	if (not BarMath_CharSettings) then
		return;
	end
	if (not BarMath_CharSettings[BarMath_Realm]) then
		return;
	end
	if (not BarMath_CharSettings[BarMath_Realm][BarMath_Faction]) then
		return;
	end
	if (not BarMath_CharSettings[BarMath_Realm][BarMath_Faction][BarMath_PlayerName]) then
		return;
	end
	local myvars = BarMath_CharSettings[BarMath_Realm][BarMath_Faction][BarMath_PlayerName];
	local value, error = "", "";

	if (lvl5 and lvl5 ~= "" and myvars[lvl1][lvl2][lvl3][lvl4][lvl5] and myvars[lvl1][lvl2][lvl3][lvl4][lvl5] ~= nil) then
		value = myvars[lvl1][lvl2][lvl3][lvl4][lvl5];
	elseif (lvl4 and lvl4 ~= "" and myvars[lvl1][lvl2][lvl3][lvl4] and myvars[lvl1][lvl2][lvl3][lvl4] ~= nil) then
		value = myvars[lvl1][lvl2][lvl3][lvl4];
	elseif (lvl3 and lvl3 ~= "" and myvars[lvl1][lvl2][lvl3] and myvars[lvl1][lvl2][lvl3] ~= nil) then
		value = myvars[lvl1][lvl2][lvl3];
	elseif (lvl2 and lvl2 ~= "" and myvars[lvl1][lvl2] and myvars[lvl1][lvl2] ~= nil) then
		value = myvars[lvl1][lvl2];
	elseif (lvl1 and lvl1 ~= "" and myvars[lvl1] and myvars[lvl1] ~= nil) then
		value = myvars[lvl1];
	end

	if value == "" then
		BarMath_Msg(BARMATH_DEVERROR_NOVALUEFOUND,"DevError")
	end

	return value;
end

function BarMath_SetVar(value, lvl1, lvl2, lvl3, lvl4, lvl5)
	if (lvl5 and lvl5 ~= "") then
		BarMath_CharSettings[BarMath_Realm][BarMath_Faction][BarMath_PlayerName][lvl1][lvl2][lvl3][lvl4][lvl5] = value;
	elseif (lvl4 and lvl4 ~= "") then
		BarMath_CharSettings[BarMath_Realm][BarMath_Faction][BarMath_PlayerName][lvl1][lvl2][lvl3][lvl4] = value;
	elseif (lvl3 and lvl3 ~= "") then
		BarMath_CharSettings[BarMath_Realm][BarMath_Faction][BarMath_PlayerName][lvl1][lvl2][lvl3] = value;
	elseif (lvl2 and lvl2 ~= "") then
		BarMath_CharSettings[BarMath_Realm][BarMath_Faction][BarMath_PlayerName][lvl1][lvl2] = value;
	elseif (lvl1 and lvl1 ~= "") then
		BarMath_CharSettings[BarMath_Realm][BarMath_Faction][BarMath_PlayerName][lvl1] = value;
	end
end

function BarMath_GetInfo(lvl1, lvl2, lvl3, lvl4, lvl5, lvl6, lvl7)
	local myvars = BarMath_Info;
	local value = "";
	if (lvl7 and lvl7 ~= "") then
		value = myvars[lvl1][lvl2][lvl3][lvl4][lvl5][lvl6][lvl7];
	elseif (lvl6 and lvl6 ~= "") then
		value = myvars[lvl1][lvl2][lvl3][lvl4][lvl5][lvl6];
	elseif (lvl5 and lvl5 ~= "") then
		value = myvars[lvl1][lvl2][lvl3][lvl4][lvl5];
	elseif (lvl4 and lvl4 ~= "") then
		value = myvars[lvl1][lvl2][lvl3][lvl4];
	elseif (lvl3 and lvl3 ~= "") then
		value = myvars[lvl1][lvl2][lvl3];
	elseif (lvl2 and lvl2 ~= "") then
		value = myvars[lvl1][lvl2];
	elseif (lvl1 and lvl1 ~= "") then
		value = myvars[lvl1];
	end
	return value;	
end

function BarMath_SetInfo(value, lvl1, lvl2, lvl3, lvl4, lvl5, lvl6, lvl7)
	if (lvl7 and lvl7 ~= "") then
		BarMath_Info[lvl1][lvl2][lvl3][lvl4][lvl5][lvl6][lvl7] = value;
	elseif (lvl6 and lvl6 ~= "") then
		BarMath_Info[lvl1][lvl2][lvl3][lvl4][lvl5][lvl6] = value;
	elseif (lvl5 and lvl5 ~= "") then
		BarMath_Info[lvl1][lvl2][lvl3][lvl4][lvl5] = value;
	elseif (lvl4 and lvl4 ~= "") then
		BarMath_Info[lvl1][lvl2][lvl3][lvl4] = value;
	elseif (lvl3 and lvl3 ~= "") then
		BarMath_Info[lvl1][lvl2][lvl3] = value;
	elseif (lvl2 and lvl2 ~= "") then
		BarMath_Info[lvl1][lvl2] = value;
	elseif (lvl1 and lvl1 ~= "") then
		BarMath_Info[lvl1] = value;
	end
end

-- Set Text function. Determines what to set each text to.
function BarMath_SetBarText(Text, Bar, Slot)
	getglobal("BarMath"..Bar.."BarDisplay"..Slot.."Text"):SetText(Text);
end

-- Adds Bars to the Global Listing
function BarMath_AddBar(BarTitle, Update_Func, Defaults, parent, Info_Delete_Func, BarTemplate)
	if (BarTitle == nil) then
		BarMath_Msg(BARMATH_DEVERROR_ADDBAR_TITLENOTSET, "DevError")
		return;
	end
	if (not Update_Func) then
		BarMath_Msg(BARMATH_DEVERROR_ADDBAR_NOUPDATEFUNC_PRETITLE..BarTitle..BARMATH_DEVERROR_ADDBAR_NOUPDATEFUNC_POSTTITLE, "DevError")
		return;
	end
	if (not Defaults) then
		--BarMath_Msg("Default for "..BarTitle.." not specified. Using Generics.");
		Defaults = BarMath_Defaults["Bars"];
	end
	if (not parent) then
		parent = "";
	end
	if (not BarTemplate) then
		BarTemplate = "";
	end

	BarMath_Generate_Database();

	if (not BarMath_GetVar("Bars",BarTitle.."Bar")) or (getglobal("BarMath_"..BarTitle.."_Version") and (getglobal("BarMath_"..BarTitle.."_Version") < getglobal("BarMath_"..BarTitle.."_DBRewrite_Version"))) then
		BarMath_SetVar(Defaults, "Bars",BarTitle.."Bar");
	end

	if (BarMath_GetVar("Bars",BarTitle.."Bar","Version") and (BarMath_GetVar("Bars",BarTitle.."Bar","Version") < getglobal("BarMath_"..BarTitle.."_Version"))) then
		BarMath_AddonOptionUpdate(BarTitle, Defaults);
	end

	if (not getglobal("BarMath"..BarTitle.."Bar")) then
		BarMath_MakeBar(BarTitle, parent, Update_Func, BarTemplate);
	end

	if (Info_Delete_Func ~= nil) then
		BarMath_BarInfoDelete[BarTitle] = Info_Delete_Func;
	end

	getglobal(Update_Func.."()");
	BarMath_Move();
end

-- Call this to MAKE the bars. (Replaces XML data!)
function BarMath_MakeBar(bartitle, parent, Update_Func, BarTemplate, Info_Delete_Func)
	if ((not BarTemplate) or (BarTemplate == "")) then
		BarTemplate = "BarMath_Bar_Template";
	end
	--BarMath_Msg("Making Bar "..bartitle.."...")
	local offset = BarMath_Bar_Height; -- Should always be equal to the template's height!
	local trueparent, y;

	--BarMath_Msg("BarCount: "..BarMath_Barcount);

	if (BarMath_Barcount == 0) then
		trueparent = "BarMath_ParentFrame";
		y = -BarMath_Bar_Height;
	else
		if (not parent or parent=="") then
			parent = BarMath_BarList[BarMath_Barcount]["Name"];
		end
		trueparent = "BarMath"..parent;
		y = -offset;
	end

	--BarMath_Msg(bartitle.."'s Parent is "..trueparent..", and it's offset is "..y);

	local f = CreateFrame("Frame", "BarMath"..bartitle, getglobal("BarMath_ParentFrame"), BarTemplate);
	f:ClearAllPoints();
	f:SetPoint("TOP", trueparent, "TOP", 0, y);
	f:SetFrameStrata("HIGH");
	getglobal("BarMath"..bartitle.."Bar"):SetScript("OnEnter", function() BarMath_Bar_ShowHide(bartitle, "true"); end);
	getglobal("BarMath"..bartitle.."Bar"):SetScript("OnLeave", function() BarMath_Bar_ShowHide(bartitle); end);
	f:Show();
	--BarMath_Msg(bartitle.." isvisible: "..f:IsVisible());

	BarMath_Barcount = BarMath_Barcount + 1;

	BarMath_BarList[BarMath_Barcount] = {
		["Name"] = bartitle,
		["Parent"] = trueparent,
		["Update"] = Update_Func,
		["InfoDelete"] = Info_Delete_Func,
	}

	BarMath_HaveLoaded["Bars"][bartitle] = 1;
end

function BarMath_Generate_Options(BarTitle,displayname,pulldown,tabs)
	if (not BarTitle) then
		BarMath_Msg(BARMATH_DEVERROR_NOBARTITLE,"DevError");
	elseif (BarMath_Option_Addons[BarTitle]) then
		BarMath_Msg(BARMATH_DEVERROR_ADDONOPTIONSEXIST,"DevError");
		return;
	end

	BarMath_Option_Addons[BarTitle] = {
		["Title"] = displayname,
		["Pulldown"] = pulldown,
		["Tabs"] = {},
	};
	
	if (#tabs > 0) then
		BarMath_Option_Addons[BarTitle]["Tabs"] = tabs;
	end
end

-- Remove this Bar.
function BarMath_KillBar(bartitle)
	if (not bartitle) then
		BarMath_Msg(BARMATH_DEVERROR_KILLBAR_NOBAR,"DevError");
		return;
	end
	--BarMath_Msg("Killing bar \""..bartitle.."\"...");
	BarMath_HideBar(bartitle);

	local thisframe;
	local bcount = count(BarMath_BarList);
	for x=1,bcount do
		if (BarMath_BarList[x]["Name"] == bartitle) then
			thisframe = x;
			break;
		end
	end
	for y=thisframe, bcount do
		if (y == bcount) then
			BarMath_BarList[y] = nil;
		else
			BarMath_BarList[y] = BarMath_BarList[(y+1)];
		end
	end
	BarMath_Move();
end

function BarMath_HideBar(bartitle)
	local f = getglobal("BarMath"..bartitle);
	f:Hide();
end

function BarMath_ShowHideDisplay(Bar, Slot, enter)
	if (not enter) then
		enter = "false";
	end;

	if ((BarMath_GetVar("Bars",Bar.."Bar","DisplayMethod") == 1) or (enter == "true")) then
		if (getglobal("BarMath"..Bar.."BarDisplay"..Slot.."Text"):GetText() ~= "nil") and (BarMath_GetVar("Bars",Bar.."Bar","Displays","Display"..Slot.."OnOff") == 1) then
			getglobal("BarMath"..Bar.."BarDisplay"..Slot.."Text"):Show();
		else
			getglobal("BarMath"..Bar.."BarDisplay"..Slot.."Text"):Hide();
		end
	else
		getglobal("BarMath"..Bar.."BarDisplay"..Slot.."Text"):Hide();
	end
end

-- Add Event
function BarMath_AddEvent(Event, Function)
	if (not Event) then
		BarMath_Msg(BARMATH_DEVERROR_EVENT_NOEVENT,"DevError");
		return;
	end
	if (not Function) then
		BarMath_Msg(BARMATH_DEVERROR_EVENT_NOFUNCTION..Event.."\".","DevError");
		return;
	end
	--BarMath_Msg("Registering Event \""..Event.."\" with function \""..tostring(Function).."\".");
	local event = tostring(Event);
	if (not BarMath_EventList[event]) then
		BarMath_EventList[event] = {};
		--BarMath_Msg("Event not in List. Generating...");
	end
	local c_count = #BarMath_EventList[Event];
	BarMath_EventList[event][(c_count+1)] = Function;
end

-- Master Show/Hide function. This will determine if a Bar, and all it's elements should be seen or not.
function BarMath_Bar_ShowHide(barname, enter)
	if (not enter) then
		enter = "false";
	end;

	if (BarMath_GetVar("Bars",barname.."Bar","Display") == 1) then
		getglobal("BarMath"..barname):Show();
	else
		getglobal("BarMath"..barname):Hide();
	end

	-- If the bar is hidden, don't bother show/hiding the rest.
	if (getglobal("BarMath"..barname):IsVisible()) then
		for x=1,BarMath_DisplayCount do
			BarMath_ShowHideDisplay(barname, x, enter);
		end
	end
	BarMath_ReorderBars(BarMath_CurrentBarOrder);
end


function BarMath_Round(num, decimal, mode)
	if (not decimal) then
		decimal = BarMath_GetVar("Decimals");
	end
	if (not mode) then
		mode = BarMath_GetVar("Rounding");
	end
	local idp = tonumber(decimal);
	local mult = 10^(idp or 0);
	BarMath_Msg('Rounding "'..tostring(num)..'" '..tostring(mode)..', to the nearest "'..tostring(idp)..'" spot.',"debug");
	if (mode == BARMATH_ROUNDING_UP) then
		result = math.ceil(num * mult) / mult;
	elseif (mode == BARMATH_ROUNDING_STANDARD) then
		result = math.floor(num * mult + 0.5) / mult;
	else
		result = math.floor(num * mult) / mult;
	end
	BarMath_Msg('Rounding Result: "'..result..'"',"debug");
	return result;
end

--==Bar Generation & control Functions==--

-- Reset the ParentBar, in case of trouble!
function BarMath_ParentBar_Reset(bypass)

	if (bypass == nil) then
		BarMath_ReorderBars(BarMath_CurrentBarOrder);
	end

	BarMath_ParentFrame:SetPoint("BOTTOM", MainMenuBar, "TOP", 0, -BarMath_Bar_Height);
	--BarMath_ParentFrame:SetPoint("TOP", MainMenuBar, "TOP", 0, 0);
	BarMath_ParentFrame:SetUserPlaced(false);
	BarMath_BarAdjust();
end

-- Command Line handler
function BarMath_commandline(cmd)
	cmd = strlower(cmd);
	if (not BarMath_CharSettings) then
		BarMath_Generate_Database();
	end

	if (cmd == BARMATH_CMD_HELP) then
		BarMath_Msg(BARMATH_CMDHELP_01);
		BarMath_Msg(BARMATH_CMDHELP_02);
		BarMath_Msg(BARMATH_CMDHELP_03);
	elseif (cmd == BARMATH_CMD_RESET) then
		BarMath_ParentBar_Reset();
	elseif (cmd == BARMATH_CMD_FIX) then
		BarMath_BarFix();
	elseif (cmd == BARMATH_CMD_COMMAS_ON) or (cmd == BARMATH_CMD_COMMA_ON) then
		BarMath_SetVar(1, "UseCommas");
		BarMath_Update_All();
	elseif (cmd == BARMATH_CMD_COMMAS_OFF) or (cmd == BARMATH_CMD_COMMA_OFF) then
		BarMath_SetVar(0, "UseCommas");
		BarMath_Update_All();
	elseif (cmd == BARMATH_CMD_ROUND.." "..BARMATH_CMD_ROUND_STANDARD) then
		BarMath_SetVar(BARMATH_ROUNDING_STANDARD,"Rounding");
		BarMath_Update_All();
	elseif (cmd == BARMATH_CMD_ROUND.." "..BARMATH_CMD_ROUND_UP) then
		BarMath_SetVar(BARMATH_ROUNDING_UP,"Rounding");
		BarMath_Update_All();
	elseif (cmd == BARMATH_CMD_ROUND.." "..BARMATH_CMD_ROUND_DOWN) then
		BarMath_SetVar(BARMATH_ROUNDING_DOWN,"Rounding");
		BarMath_Update_All();
	elseif (cmd == BARMATH_CMD_MINIMAP.." "..BARMATH_CMD_OFF) then
		BarMath_SetVar(0,"ShowMinimapButton");
		BarMath_Minimap_OnOff();
	elseif (cmd == BARMATH_CMD_MINIMAP.." "..BARMATH_CMD_ON) then
		BarMath_SetVar(1,"ShowMinimapButton");
		BarMath_Minimap_OnOff();
	elseif (cmd == BARMATH_CMD_CUTOFF_GOLD) then
		BarMath_SetVar("g","Bars","MoneyBar","Cutoff");
		BarMath_Money_Update();
		--BarMath_Msg("Now only Gold will be shown.");
	elseif (cmd == BARMATH_CMD_CUTOFF_SILVER) then
		BarMath_SetVar("s","Bars","MoneyBar","Cutoff");
		BarMath_Money_Update();
		--BarMath_Msg("Now only Silver and Gold will be shown.");
	elseif (cmd == BARMATH_CMD_CUTOFF_COPPER) or (cmd == BARMATH_CMD_CUTOFF_OFF) then
		BarMath_SetVar("c","Bars","MoneyBar","Cutoff");
		BarMath_Money_Update();
	elseif (cmd == "cleanup") then
		BarMath_XP_LevelUp();
	elseif (strsub(cmd,1,strlen(BARMATH_CMD_DECIMAL)) == BARMATH_CMD_DECIMAL) then
		local temp = tonumber(strsub(cmd,strlen(BARMATH_CMD_DECIMAL)+2));
		if (temp == nil) then
			--BarMath_Msg(BARMATH_CMDHELP_02);
			return;
		end
		BarMath_Msg("Changing decimal place to: "..temp,"debug")
		BarMath_SetVar(temp,"Decimals");
		BarMath_Update_All();
	else
		if BarMath_Options:IsVisible() then
			HideUIPanel(BarMath_Options);
		else
			ShowUIPanel(BarMath_Options);
		end
	end
end

-- Let's us sort tables!
-- Copy/Pasted from LUA Website
function BarMath_PairsByKeys (t, f)
	local a = {}
		for n in pairs(t) do table.insert(a, n) end
		table.sort(a, f)
		local i = 0      -- iterator variable
		local iter = function ()   -- iterator function
			i = i + 1
			if a[i] == nil then return nil
			else return a[i], t[a[i]]
			end
		end
	return iter
end


-- Timer function
function BarMath_Timer(name,targettime,functiontodo)
	local framename = "BarMathTimeFrame"..name;
	if getglobal(framename) and (getglobal(framename):GetScript("OnUpdate") ~= nil) then
		return;
	end

	local frame = CreateFrame("Frame", framename);
	
	local totalElapsed = 0.0;
	local tickcount = 0;

	local function BarMathTimer_onUpdate(self, elapsed)
		totalElapsed = totalElapsed + elapsed;
		if (totalElapsed < 1) then return; end
		totalElapsed = totalElapsed - floor(totalElapsed);
		tickcount = tickcount + 1;

		if (tickcount == targettime) then
			functiontodo();
			frame:SetScript("OnUpdate", nil);
			return;
		end
		-- Enable for timing debugging
		-- ChatFrame1:AddMessage("tick... "..tickcount);
	end
	frame:SetScript("OnUpdate", BarMathTimer_onUpdate);
	frame:Show();
end

-- Add Comma function
function BarMath_AddCommas(input)
	if (BarMath_GetVar("UseCommas") == 0) then
		return input;
	end
	if (type(input) ~= "number") then
		return input;
	end

	--BarMath_Msg("Add Comma Input:"..tostring(input))

	local l = strlen(input);
	local d = strfind(input, "%.");
	local newstr, ext;

	--BarMath_Msg("Input Len:"..tostring(l)..", Dot:"..tostring(d));

	if (d) and (d ~= 1) then
		ext = strsub(input, d);
		input = strsub(input, 1,d-1);
		l = d-1;
	end

	-- BarMath_Msg("Input Len, with D:"..tostring(l));

	if l < 4 then
		if ext then
			return input..ext;
		else
			return input;
		end
	end

	--BarMath_Msg("Starting Cycle with input:"..tostring(input).."...");

	for i=l, 1, -3 do
		local x = i-2;
		if x < 1 then x = 1; end
		--BarMath_Msg("Cycle i:"..i.." to "..x);
		local sub = strsub(input, x, i);
		--BarMath_Msg("Sub:"..tostring(sub));
		
		if (newstr == nil) then
			newstr = sub;
			if ext then
				newstr = newstr..ext;
			end
		else
			newstr = sub..","..newstr;
		end
	end

	--BarMath_Msg("Add Comma Output:"..tostring(newstr))

	return newstr;
end