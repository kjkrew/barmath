--[[
	BarMath 2.0.5
	Post-Load Functions

	These functions are designed to control all the Bars, and thus, must come last.
]]

local bars;
local locked = false;
local lastXPos, lastYPos = 0, 0;

function BarMath_Post_Load()
	BarMathTitleText:SetText(BARMATH_TITLE.." "..BARMATH_TXT_VERSION_SMALL..BarMath_Version);
	if (BarMath_GetVar("ShowParentBar") == 0) then
		BarMathTitle:Hide();
	else
		BarMathTitle:Show();
	end
	BarMath_Post_SetParentHeight();
	BarMath_BarAdjust();
	BarMath_ReorderBars(BarMath_CurrentBarOrder)
	BarMath_ParentFrame:SetUserPlaced(true);
end

function BarMath_RebuildList(direction)
	if (InCombatLockdown()) or (MainMenuBar.busy == true) then
		return;
	end
end

-- Re-Orders the bars.
-- Direction is used for reversing the Bar Order. Values = 1 or -1;
function BarMath_ReorderBars(direction)
	if (InCombatLockdown()) or (MainMenuBar.busy == true) or (MainMenuBar.state ~= "player") then
		-- Error is currently outputted LOTS of times, so in-combat, it silently fails.
		-- BarMath_Msg("|CFFFF0000"..BARMATH_ERROR_INCOMBAT..FONT_COLOR_CODE_CLOSE);
		return;
	end

	local align, palign;

	if (not direction) then
		if (not BarMath_CurrentBarOrder) then
			direction = -1
		else
			direction = BarMath_CurrentBarOrder;
		end
	end
	if (direction <= 0) then
		direction = -1;
		align, palign = "TOP", "BOTTOM";
	elseif (direction >= 1) then
		direction = 1;
	end

	BarMath_CurrentBarOrder = direction;

	BarMath_Msg("Reordering Bars...","debug");

	local offset = BarMath_Bar_Height * direction;
	local titleoffset = 0;

	if (BarMath_GetVar("ShowParentBar") == 0) then
		BarMathTitle:Hide();
		titleoffset = 1;
	else
		BarMathTitle:Show();
		titleoffset = 0;
	end

	local visible = {};
	local vx = 1;

	for x=1,#(BarMath_BarList) do
		cbar = BarMath_BarList[x];

		if (BarMath_GetVar("Bars",cbar["Name"].."Bar","Display") == 1) and (getglobal("BarMath"..cbar["Name"].."Bar"):IsVisible()) then
			visible[vx] = x;
			vx = vx+1;
		end
	end

	BarMath_Msg("Visible count: "..#visible,"debug");

	for bar=1,#(visible) do
		cbar = BarMath_BarList[visible[bar]];
		local pbaroffset = BarMath_GetVar("ShowParentBar");
		if (bar == 1) then
			finaloffset = 1+pbaroffset;
			align = "BOTTOM";
			palign = "TOP";
		else
			finaloffset = bar+pbaroffset;
			align = "BOTTOM";
			palign = "TOP";
		end
		finaloffset = finaloffset*offset;
		BarMath_Msg("For Bar:"..cbar["Name"]..", Offset:"..offset..", FinalOffset:"..finaloffset,"debug");

		cf = getglobal("BarMath"..cbar["Name"]);
		cf:Show();
		cf:ClearAllPoints();
		cf:SetPoint(align, BarMath_ParentFrame, palign, 0, finaloffset);
		cf:SetAlpha(1);
		cf:SetFrameStrata("HIGH");
		getglobal(BarMath_BarList[bar]["Update"].."()");
	end

	BarMath_Move();
	BarMath_Msg("Finished Reordering Bars.","debug");
end

function BarMath_Post_SetParentHeight()
	local offset, bars = 1, 0;
	if (BarMath_GetVar("ShowParentBar") == 0) then
		offset = 0;
	end

	for x=1,#(BarMath_BarList) do
		if getglobal("BarMath"..BarMath_BarList[x]["Name"].."Bar"):IsVisible() then
			bars = bars + 1;
		end
	end

	-- Include Title Bar
	if (bars == 0) then
		bars = offset;
	else
		bars = bars + offset;
	end

	-- If Absolutely no bars, then hide the parent frame, else show it.
	if (bars == 0) then
		BarMath_ParentFrame:Hide();
		return;
	else
		BarMath_ParentFrame:Show();
	end

	local h = BarMath_Bar_Height*bars;

	BarMath_ParentFrame:SetHeight(h);
end

-- Adjust Non-BarMath bars
function BarMath_BarAdjust()
	if (not UIPARENT_MANAGED_FRAME_POSITIONS["BarMath_ParentFrame"]) then
		UIPARENT_MANAGED_FRAME_POSITIONS["BarMath_ParentFrame"] = {
			baseY = 0,
			bottomLeft = 45, 
			anchorTo = "MainMenuBar",
			point = "BOTTOMLEFT", 
			rpoint = "BOTTOMLEFT",
		};
	end
	local BarMathYOffset = BarMath_ParentFrame:GetHeight() - BarMath_Bar_Height;

	UIPARENT_MANAGED_FRAME_POSITIONS["MultiBarBottomLeft"]["anchorTo"] = "BarMath_ParentFrame";
	UIPARENT_MANAGED_FRAME_POSITIONS["MultiBarBottomLeft"]["baseY"] = 4;
	UIPARENT_MANAGED_FRAME_POSITIONS["ShapeshiftBarFrame"]["yOffset"] = BarMathYOffset;
	UIPARENT_MANAGED_FRAME_POSITIONS["PossessBarFrame"]["yOffset"] = BarMathYOffset;
	UIPARENT_MANAGED_FRAME_POSITIONS["MultiCastActionBarFrame"]["yOffset"] = BarMathYOffset;
	UIPARENT_MANAGED_FRAME_POSITIONS["PETACTIONBAR_YPOS"]["baseY"] = 97+BarMathYOffset;

	-- Parent to MainMenuBar if not user-placed
	if (BarMath_ParentFrame:IsUserPlaced() ~= true) then
		BarMath_ParentFrame:SetPoint("BOTTOM", MainMenuBar, "TOP", 0, -BarMath_Bar_Height);
	end

	-- Update Right-Side Frames
	UIParent_ManageFramePositions();

	-- Kill the other bars
	MainMenuExpBar:Hide();
	MainMenuBarMaxLevelBar:Hide();
	ReputationWatchBar:Hide();

	-- Update Bags
	updateContainerFrameAnchors();
end

function BarMath_Move()
	if (not InCombatLockdown()) and (MainMenuBar.state == "player") and (MainMenuBar.busy == false) then
		if (not BarMath_ParentFrame:IsVisible()) then
			return;
		end

		local xpos,ypos = BarMath_ParentFrame:GetLeft(), BarMath_ParentFrame:GetTop();
		local screenx = GetScreenWidth();
		local screeny = GetScreenHeight();
		local PBarW = BarMath_ParentFrame:GetWidth();
		local leeway = 25;

		-- Reset the bar, if it goes beyond the bounds of the screen.
		if (xpos-leeway < -PBarW) or (xpos+leeway > screenx) or (ypos > screeny) or (ypos-leeway < 0) then
			BarMath_ParentBar_Reset(1);
		end

		-- If something breaks, then reset the X & Y.
		if (xpos == nil or ypos == nil) then
			if (lastXPos == 0 and lastYPos == 0) then
				BarMath_ParentBar_Reset();
				xpos,ypos = BarMath_ParentFrame:GetLeft(), BarMath_ParentFrame:GetTop();
			else
				xpos,ypos = lastXPos, lastYPos;
			end
		end

		local MMBCenterX = (MainMenuBar:GetLeft() + MainMenuBar:GetRight())/2;
		local MMBTop = MainMenuBar:GetTop();
		local variable = BarMath_ParentFrame:GetHeight();

		-- Defaults to Not Snapped.
		locked = false;

		if (BarMath_GetVar("AllowSnapToZones") == 1) then
			-- Standard Position
			if (MainMenuBar:IsVisible()) then
				if (ypos<(MMBTop+variable) and ypos>(MMBTop-variable)) then
					locked = true;
					BarMath_ParentFrame:ClearAllPoints();
					BarMath_ParentFrame:SetPoint("BOTTOM", MainMenuBar, "TOP", 0 , -BarMath_Bar_Height);
				end
			end
		end
		lastXPos, lastYPos = BarMath_ParentFrame:GetLeft(), BarMath_ParentFrame:GetTop();
		BarMath_ParentFrame:SetUserPlaced(true);
		BarMath_Post_SetParentHeight();
		BarMath_BarAdjust();
	end
end

function BarMath_BarFix()
	securecall(MainMenuBar_UpdateArt,MainMenuBar);
	if (MainMenuBar.state == "player") then
		BarMath_BarAdjust();
	end
end