--[[
	BarMath
	Constants
	Version <%version%>
	
	Revision: $Id: Constants.lua 3 2012-11-17 19:47:10 PST Kjasi $
]]

-- Initialize our Addon
BarMath = {}
local bm = BarMath

--== Variables ==--
bm.NumBars = 20		-- Number of Bars per Level
bm.DisplayCount = 5	-- Number of data displays per bar
bm.BarHeight = 12	-- Height of each bar in Pixels