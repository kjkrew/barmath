BarMath v<%version%>

-= Troubleshooting =-

- Character List - 

Q: HELP! I accidently deleted the data from a character of mine! What do I do?!
A: Calm down. Sadly, you have lost all of that character's personal settings. However, you can easily re-set those settings by logging into that character, and changing their BarMath settings. Some addons, such as the Money Bar, will automatically rebuild their own data.

Q: My Bars all disappeared! Where'd they go?
A: If your bars disappear, then either use the commandline "/barmath reset" or open the Options window and hit the "Reset Parent Bar" button in the upper right.

Q: I thought this version had moveable bars, but I can't seem to move them!
A: The bars can only be moved while out of combat, and when the Parent Bar is visible. Open the Options window, and make sure "Show Parent Bar" is selected, then you can click and drag the parent bar to move your bars anywhere. *Note* There is currently only 1 Snap-To zone, and that's where the bars currently are. More Snap-To zones will be added in subsequent releases of BarMath.


-= Command-line List =-
/barmath = Opens and closes the options window.
	decimal # = The number of decimal points to display on the bars.
	reset = This will reset the position of the Parent Bar to it's default location. Use this if you lose your Bars.
	fix = Attempts to fix most of the problems caused by reloading the Main Menu bar, after leaving a vehicule.
	cutoff [gold | silver | copper | off] = Round to the nearest options selected, and display that. (Money Bar Only.)


-= Version History =-
v<%version%>
 - Updated for 5.1

BarMath v2.1 Alpha 2
 - 4.3 Update.

BarMath v2.1 Alpha 1
 - Rewritten from scratch for Cataclysm!
 - Fixed BarMath issues with vehicles!
 - BarMath now supports the use of Commas in bar data! XP, Money and Rep have been updated to reflect this!
 - Added a PetXP Bar for Hunters, wanting to get quick information about their pet's level.

BarMath v2.0.5
 - Fixed memory leak.

BarMath v2.0.4
- FINALLY Fixed most of the major Bar Problems!
- Updated for Patch 3.2!
- Added a commandline to make it simple to correct any problems when exiting a vehicule.
- You can now delete data for toons on other realms that have the same name as your current toon.
- Extended the OnEvent listing to support every argument, not just the first 5.
- Fixed a bug that would occur while working with vehicles.
- Fixed a bug that would occur when no title or text is sent to the BarMath_Tooltip_Generate function.

BarMath v2.0.3
 - Fixed the bug that wouldn't show the bars until you turned the parent bar on and off again.
 - Fixed a bug that would call the wrong function while attempting to change the cut-off setting.
 - Fixed several command-line bugs.

BarMath v2.0.2
 - Extended the Parent Bar's "Grab Zone".
 - Reduced the size of the SnapZone for the MainMenuBar.
 - Fixed a bug that would cause a stack overflow if you moved the Parent Bar too far off screen.
 - Fixed a database update bug.
 - Fixed an error that would cause the castbar, petbar and shape-shift bars not to be in the proper places.
 - Known Issue: When first logging on, the BarMath Bars will not appear until you update their data, or (a much faster method) turn the parent bar on and off.

BarMath v2.0.1
 - Added an option to turn off "Snap To" zones. (More zones to be added soon!)
 - Fixed a bug that would make the parent bar appear WAY off-screen the first time you load the addon, effectively hiding it from the screen.

BarMath v2.0
 - Updated for Wrath of the Lich King!
 - Rewrote most of the code to make it run better, smoother, faster.
 - Made Major code changes, to allow for easy adding of additional bars.
 - Separated out the bars, and added them to their own floating Bar, which you can place anywhere.
 - Added a "snap to" zone. So if you place the BarMath bars near where the original bars were at, it will "snap" into place.
 - Added a Character List, in order to manage each character's data. Also allows you to delete the data of characters you've deleted.
 - Major layout changes to the Options panel, to allow for the new options.
 - Added functionality so other addons can add their own bars.
 - Broke out XP, Rep and Money into their own Addons.


Included Addon Version History is available in their Readme Files.


-= Special Thanks =-
The following people have been really helpful in troubleshooting some issues:

Thorghast - Steamwheedle Cartel (US)


-= Developer FAQ =-

Q: How do I develop an addon for BarMath?
A: I've done my best to document the XP, Rep and Money addons, so just browse through those, and you should figure it out.

Q: How do I add a command-line option?
A: At the moment, you don't. This functionality will be added in BarMath 2.1.